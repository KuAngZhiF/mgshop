<?php 
//配置伪静态
Route::get('/','Home/Index/index');

//列表页
Route::get('H_L_i_{cid}.html','Home/Lists/index');
//详情页
Route::get('H_D_i_{gid}.html','Home/Details/index');
//购物车
Route::get('H_C_i.html','Home/Cart/index');
//购物车表单提交
Route::post('C_i.html','Home/Cart/index');
//我的购物车首页
Route::get('H_C_i.html','Home/Cart/index');
//确认订单
Route::get('H_C_a_{sid}.html','Home/Cart/affirmCart');
//支付
Route::get('H_C_ali_{oid}.html','Home/Cart/alipay');

//我的订单
Route::get('H_U_mt_{s}.html','Home/Userinfo/myIndent');
//订单详情
Route::get('H_U_odd_{oid}.html','Home/Userinfo/orderdetail');
//个人中心
Route::get('H_U.html','Home/Userinfo/userinfo');
//修改头像
Route::get('H_U_face.html','Home/Userinfo/editFace');
//用户地址管理
Route::get('H_U_site_{sid}.html','Home/Userinfo/site');

//搜索页
Route::get('H_S.html','Home/Search/index');

//用户登录界面
Route::get('H_log.html','Home/Login/login');
Route::get('H_reg.html','Home/Login/register');
Route::get('H_logout.html','Home/Login/LoginOut');