<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>蘑菇街-首页</title>
		<!--载入头部-->
				<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/common.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/floor.css"/>
		
		<!-- 载入HDjs样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css"/>
		<!-- 首页样式只有首页有 -->
		<?php if( 'Index'=='Index' && 'index'=='index' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/index.css"/>
		
               <?php }?>
		
		<?php if( 'Index'=='Userinfo' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/userinfo.css"/>
		
               <?php }?>
		
		<?php if( 'Index'=='Lists' ){?>
                
		<!-- 列表页样式  -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/lists.css"/>
		
               <?php }?>
		
		<?php if( 'Index'=='Details' ){?>
                
		<!-- 详情页 样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/details.css"/>
		
               <?php }?>
		
		<!-- 购物车样式 -->
		<?php if( 'Index'=='Cart' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/cart.css"/>
		
               <?php }?>
		
		<?php if( 'Index'=='Userinfo' && 'index'=='orderdetail' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/indent.css"/>
		
               <?php }?>
		
		<!-- 搜索页样式 -->
		<?php if( 'Index'=='Search' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/seek.css" />
		
               <?php }?>
		
		<!-- 载入上传Uploadify样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">
		
		<script type="text/javascript">
				var Public = "<?php echo __PUBLIC__?>";
				var Root = "<?php echo __ROOT__?>";
				var sname = "<?php echo session_name();?>";
				var sid = "<?php echo session_id();?>";
				var userFaceUrl = "<?php echo U('Userinfo/face')?>";
				var payValueUrl = "<?php echo U('Common/payValue')?>";
		</script>
		
		
	</head>
	<body>
		<!-- HD模态框  开始 -->
		<div id="hd-modal-bg" style="opacity:0.7;filter:alpha(opacity=7);display:none;">背景遮罩</div>
		<div id="hd-modal" style="width:300px;height:180px;margin-left:-180px; display:none;">        
				<div class="hd-modal-wrap" style="height:180px">            
						 <div class="hd-modal-header">                
							<span class="hd-modal-title">余额充值</span>               
						 	<span class="hd-modal-close hd-clone-modal"></span>            
						  </div>            
				 		  <div class="hd-modal-message">
				 		  		<textarea name="pay" rows="4" id="text-area"  style="width:280px;" class="text-pay-value"></textarea>
				 		  </div>           
						  <div class="hd-modal-btn">                
							  <a class="hd-btn hd-btn-success hd-success-modal" href="javascript:;">确认充值</a>&nbsp;                
							  <a class="hd-btn hd-clone-modal" href="javascript:;">关闭</a>           
						   </div>        
				</div>    
		</div>
		<!-- HD模态框  结束 -->
		
		<!--公共头部样式 开始-->
		<!--头部 开始-->
		<div class="header-area">
			<div class="header">
				<a href="<?php echo __ROOT__?>" class="home">蘑菇街首页</a>
				<ul class="header-top">
					<!--用户名登录后 的div-->
					<?php if(isset($_SESSION['uid'])?$_SESSION['uid']:0){?>
                
					<li class="t1 has_icon user_meta" id="user_meta">
	                    <a href="javascript:;">很纯很暧昧777</a>
	                    <a href="" target="_blank"><span class="user-level user-level0">&nbsp;</span></a>
	                    <i class="icon_delta"></i>
	                    <ol class="ext_mode" id="menu_personal" style="display: none;">
	                        <li class="s2"><a target="_blank" href="H_U.html">个人设置</a></li>
	                        <li class="s2"><a target="_blank" href="H_U.html">账号绑定</a></li>
	                        <li class="s2"><a rel="nofollow" href="H_logout.html">退出</a></li>
	                    </ol>
                	</li>
                	<!--用户名登录后 的div 结束-->
                	<?php }else{?>
                	<!--没登录状态-->
					<li class="t1"><a href="H_reg.html">注册</a></li>
					<li class="t1"><a href="H_log.html">登录</a></li>
					<!--没登录状态 结束-->
					
               <?php }?>
					<li class="t1 myorder t1-line" id="J-order" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>"><a href="javascript:;">我的订单</a></li>
					<li class="t1 myorder t1-cate" id="t1-cate">
						<a href="H_C_i.html">购物车
							<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
							<span class="floorNum" style="color: #f36;padding: 0px;font-weight: 400;"><?php echo $_SESSION['tatol']?></span>
							<span>件</span>
							
               <?php }?>
						</a>
						<!--购物车隐藏盒子 开始-->
						
						<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
						<!--有商品的时候-->
						<div class="cate-hide cate-info" style="display: none;">
							<ul>
								<?php foreach ($_SESSION['goods'] as $v){?>
	                            <li>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="imgbox">
										<img src="<?php echo __ROOT__?>/<?php echo $v['pic']?>"  width="45" height="45">
									</a>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="title"><?php echo $v['gname']?></a>
									<span class="info">
											<?php foreach ($v['options'] as $gtname=>$vo){?>
									    	<span><?php echo $gtname?>：<?php echo $vo?></span>
									    	<?php }?>
									</span>
									<span class="price">￥<?php echo $v['shopprice']?></span>
									<a href="javascript:;" class="del delete" mgprice="<?php echo $v['price']?>" glid="<?php echo $v['glid']?>">删除</a>
								</li>
								<?php }?>
								
							</ul>
							<div class="subbox">
                    			<div class="fr">
                				<a href="H_C_i.html" target="_blank" class="goel">查看购物车</a>
            					</div>
        					</div>
						</div>
						<!--购物车隐藏盒子 结束-->
						<?php }else{?>
						<!--没有商品的时候-->
						<div class="cate-hide empty_cart" style="display: none;">
							购物车里没有商品！
						</div>
						<!--没有商品的时候 结束-->
						
               <?php }?>
					</li>
				</ul>
				
			</div>
		</div>
		<!--头部 结束-->
		
		<!--搜索区域   开始-->
		<div class="search-warp">
			<div class="search-area">
			<div class="logo">
				<a href="<?php echo __ROOT__?>" title="蘑菇街首页"></a>
			</div>
			
			<!--搜索框 区域-->
			<div class="search">
				<div class="search-box">
					<div class="selectbox">
                		<span class="selected">搜商品</span>
               		</div>
               		<!--form 表单 开始-->
               		<form action="H_S.html" method="get" id="sea-form">
               			<input type="text" value="<?php echo isset($_GET['words'])?$_GET['words']:'上衣'?>" class="ts-txt" name="words"/>
               			<input type="submit" value="搜  索" class="ts-btn"/>
               		</form>
               		<!--form 表单 结束-->
               	</div>
               	
               	<!--栏目列表  开始-->
               	<div class="cate-list">
               		<?php foreach ($cateData_cache as $v){?>
               		<a href="H_S.html?words=<?php echo $v['cname']?>"><?php echo $v['cname']?></a>
               		<?php }?>
               	</div>
               	<!--栏目列表 结束-->
				
			</div>
			<!--搜索框 结束-->
			
			<!--二维码 开始-->
	       		<!-- <div class="e-qrcode">
	                <img src="<?php echo __PUBLIC__?>/Home/images/ecode.png" alt="蘑菇街客户端下载" height="70" width="70">
	                <p>蘑菇街客户端</p>
	            </div> -->
       		<!--结束-->
		</div>
		</div>
		<!--搜索区域   结束-->
		
		<?php if( 'Index'=='Index' && 'index'=='index' ){?>
                
		<!--菜单列表 开始-->
		<div class="menu-area">
			<div class="menu">
				<ul class="menu-list">
					<li class="home">全部商品</li>
									<?php
					$model = new \Common\Model\Category();
					$data = $model->where('pid=0 AND is_show=1')->limit(8)->get();
					foreach($data as $field):
					//列表页地址
					$field['url'] =__ROOT__ .  "/H_L_i_". $field['cid'] . ".html";
				?>
					
					<li><a href="<?php echo $field['url']?>"><?php echo $field['cname']?></a></li>
					
				<?php endforeach;?>
				</ul>
			</div>
		</div>
		<!--菜单列表 结束-->
		
               <?php }?>
		
		<!--公共头部样式 结束-->
		
		<!--轮播 栏目列表 图片展示区域 开始-->
		<div class="primary-wrap">
			<!--栏目列表 的开始-->
			<div class="primary-nav">
				<ul class="primary-nav-list">
				<?php foreach ($cateData as $v){?>
					<li class="primary-nav-li">
						<dl class="nav-warp">
							<dt><a  target="_blank" href="<?php echo __ROOT__?>/H_L_i_<?php echo $v['cid']?>.html"><?php echo $v['cname']?></a></dt>
							<dd>
								<span>
									<?php foreach ($v['_data'] as $vvo){?>
									<?php foreach ($vvo['_data'] as $vvoo){?>
									<ins></ins><a  target="_blank" href="<?php echo __ROOT__?>/H_L_i_<?php echo $vvoo['cid']?>.html"  <?php if($vvoo['is_hot']==1){?>
                class="hot"
               <?php }?>><?php echo $vvoo['cname']?></a>
									<?php }?>
									<?php }?>
								</span>
							</dd>
						</dl>
						<!--隐藏线 遮盖线-->
						<i class="nav-line"></i>
						<!--栏目 隐藏盒子  开始-->
						<div class="nav-hide">
							<?php foreach ($v['_data'] as $vo){?>
							<dl class="nav-more-area">
								<dt><a href="<?php echo __ROOT__?>/H_L_i_<?php echo $vo['cid']?>.html"><?php echo $vo['cname']?></a></dt>
								<?php foreach ($vo['_data'] as $vv){?>
								<dd><a href="<?php echo __ROOT__?>/H_L_i_<?php echo $vv['cid']?>.html" <?php if($vv['is_hot']==1){?>
                class="hot"
               <?php }?>><?php echo $vv['cname']?></a></dd>
								<?php }?>
							</dl>
							<?php }?>
							<div class="nav-more-pic">
								<a target="_blank" href="<?php echo __ROOT__?>/H_L_i_<?php echo $v['cid']?>.html">
								<img src="<?php echo __ROOT__?>/<?php echo $v['max_imgs_250x130']?>" width="250" height="130"/></a>
							</div>
							
						</div>
						<!--栏目 隐藏盒子  结束-->
					</li>
					<?php }?>
					
				</ul>
			</div>
			<!--栏目列表 的结束-->
			
			<!--轮播图 开始-->
			<div class="slide-area">
				<div class="slide-box">
					<a href="<?php echo __ROOT__?>/H_L_i_1.html"><img src="<?php echo __PUBLIC__?>/Home/images/a1.jpg"</a>
					<a href="<?php echo __ROOT__?>/H_L_i_1.html"><img src="<?php echo __PUBLIC__?>/Home/images/a2.png"/></a>
					<a href="<?php echo __ROOT__?>/H_L_i_80.html"><img src="<?php echo __PUBLIC__?>/Home/images/a3.jpg"/></a>
					<a href="<?php echo __ROOT__?>/H_L_i_30.html"><img src="<?php echo __PUBLIC__?>/Home/images/a4.jpg"/></a>
					<a href="<?php echo __ROOT__?>/H_L_i_1.html"><img src="<?php echo __PUBLIC__?>/Home/images/a5.jpg"/></a>
					<a href="<?php echo __ROOT__?>/H_L_i_80.html"><img src="<?php echo __PUBLIC__?>/Home/images/a3.jpg"/></a>
				</div>
				<div class="hd-slide">
					<ul>
						<li class="cur">1</li>
						<li>2</li>
						<li>3</li>
						<li>4</li>
						<li>5</li>
						<li>6</li>
					</ul>
				</div>
				<a href="javascript:;" class="prev"></a>
				<a href="javascript:;" class="next"></a>
			</div>
			
			<!--轮播图 结束-->
			
			<!--右侧图片展示  开始-->
			<div class="img-right">
				<ul class="sale-list">
					<li class="sale-li">
						<a target="_blank" href="<?php echo __ROOT__?>/H_L_i_349.html" class="sale-box">
							<img src="<?php echo __PUBLIC__?>/Home/images/m1.png">
						</a>
					</li>
					
					<li class="sale-li">
						<a target="_blank" href="<?php echo __ROOT__?>/H_L_i_51.html" class="sale-box">
							<img src="<?php echo __PUBLIC__?>/Home/images/m2.png">
						</a>
					</li>
				</ul>
				
			</div>
			<!--右侧图片展示  结束-->
		</div>
		<!--轮播 栏目列表 图片展示区域 结束-->
		
		<!--首屏图片展示区域 开始-->
		<div class="special-con">
			<!--限速快抢-->
			<div class="sp-col1">
				<a href="<?php echo __ROOT__?>/H_L_i_1.html">
					<img src="<?php echo __PUBLIC__?>/Home/images/t2.png"/>
				</a>
			</div>
			<div class="sp-col1">
				<a href="<?php echo __ROOT__?>/H_L_i_1.html">
					<img src="<?php echo __PUBLIC__?>/Home/images/2p240x300.jpg"/>
				</a>
			</div>
			<div class="sp-col1 sp2">
				<a href="<?php echo __ROOT__?>/H_L_i_1.html">
					<img src="<?php echo __PUBLIC__?>/Home/images/t1.png"/>
				</a>
			</div>
			<div class="sp3">
				<a href="<?php echo __ROOT__?>/H_L_i_1.html"><img src="<?php echo __PUBLIC__?>/Home/images/t3.jpg"/></a>
				<a href="<?php echo __ROOT__?>/H_L_i_1.html" class="last"><img src="<?php echo __PUBLIC__?>/Home/images/t4.jpg"/></a>
			</div>
		</div>
		<!--首屏图片展示区域 结束-->
		
		<!--楼层 开始-->
		<div class="floor-list">
			<!--第一层 开始-->
			<div class="fr-area">
				<!--标题 开始-->
				<div class="ft-title">
					<div class="left-f">
						<div class="hbg_l">
							<div class="hbg_r">
								<div class="hbg_m"></div>
							</div>
						</div>
					</div>
					<h3 class="ht_t_con">品牌馆</h3>
					<div class="left-f">
						<div class="hbg_l">
							<div class="hbg_r">
								<div class="hbg_m"></div>
							</div>
						</div>
					</div>
				</div>
				<!--标题 结束-->
				
				<!--内容 开始-->
				<div class="fr-goods-area">
					<!--左侧排版 开始-->
					<div class="goods-left">
						<a href="<?php echo __ROOT__?>/H_L_i_1.html" class="a-up"><img src="<?php echo __PUBLIC__?>/Home/images/a-up.jpg" class="fade-in"/></a>
						
						<?php foreach ($brandData as $v){?>
						<a href="<?php echo __ROOT__?>/H_L_i_1.html" class="a-btm" title="<?php echo $v['bname']?>">
							<img src="<?php echo __ROOT__?>/<?php echo $v['logo']?>" class="fade-in"/>
							<span class="hover_cover"></span>
						</a>
						<?php }?>
						
					</div>
					<!--左侧排版 结束-->
					
					<!--右侧排版开始-->
					<div class="goods-right">
						<div class="g-pic"><a href="<?php echo __ROOT__?>/H_L_i_1.html"><img src="<?php echo __PUBLIC__?>/Home/images/de_305x500.jpg" alt="" /></a></div>
						<div class="c-goods">
							<div class="goods-list">
								<?php foreach ($goodsData1 as $v){?>
								<a href="<?php echo __ROOT__?>/H_L_i_<?php echo $v['category_cid']?>.html" class="goods-item">
									<img src="<?php echo __ROOT__?>/<?php echo $v['cate_img']?>" class="fade-in"/>
									<span class="goods-name"> 
										<span class="gd-name-ch"><?php echo $v['cate_name']?></span> 
									</span>
								</a>
								<?php }?>
							</div>
						</div>
					</div>
					<!--右侧排版结束-->
				</div>
				<!--内容结束-->
			</div>
			<!--第一层 结束-->
			
			<!--第三层 开始-->
			<div class="fr-area">
				<!--标题 开始-->
				<div class="ft-title">
					<div class="left-f">
						<div class="hbg_l">
							<div class="hbg_r">
								<div class="hbg_m"></div>
							</div>
						</div>
					</div>
					<h3 class="ht_t_con_a">享瘦</h3>
					<div class="left-f">
						<div class="hbg_l">
							<div class="hbg_r">
								<div class="hbg_m"></div>
							</div>
						</div>
					</div>
				</div>
				<!--标题 结束-->
				
				<!--内容 开始-->
				<div class="fr-goods-area">
					<!--左侧排版 开始-->
					<div class="goods-left">
						<a href="<?php echo __ROOT__?>/H_L_i_79.html" class="a-up"><img src="<?php echo __PUBLIC__?>/Home/images/bs_de_223x254.jpg" class="fade-in"/></a>
						<?php foreach ($brandData1 as $vv){?>
						<a href="<?php echo __ROOT__?>/H_L_i_79.html" class="a-btm" title="<?php echo $vv['bname']?>">
							<img src="<?php echo __ROOT__?>/<?php echo $vv['logo']?>" class="fade-in"/>
							<span class="hover_cover"></span>
						</a>
						<?php }?>
						
					</div>
					<!--左侧排版 结束-->
					
					<!--右侧排版开始-->
					<div class="goods-right">
						<div class="g-pic"><a href="<?php echo __ROOT__?>/H_L_i_1.html"><img src="<?php echo __PUBLIC__?>/Home/images/meyde_305x500.jpg" alt="" /></a></div>
						<div class="c-goods">
							<div class="goods-list">
							
								<?php foreach ($goodsData2 as $vo){?>
								<a href="<?php echo __ROOT__?>/H_L_i_<?php echo $vo['category_cid']?>.html" class="goods-item">
									<img src="<?php echo __ROOT__?>/<?php echo $vo['cate_img']?>" class="fade-in"/>
									<span class="goods-name"> 
										<span class="gd-name-ch"><?php echo $vo['cate_name']?></span> 
									</span>
								</a>
								<?php }?>
								
							</div>
						</div>
					</div>
					<!--右侧排版结束-->
				</div>
				<!--内容结束-->
			</div>
			<!--第三层 结束-->
			
			
			<!--第四层 开始-->
			<div class="fr-area">
				<!--标题 开始-->
				<div class="ft-title">
					<div class="left-f">
						<div class="hbg_l">
							<div class="hbg_r">
								<div class="hbg_m"></div>
							</div>
						</div>
					</div>
					<h3 class="ht_t_con_b">轻时髦</h3>
					<div class="left-f">
						<div class="hbg_l">
							<div class="hbg_r">
								<div class="hbg_m"></div>
							</div>
						</div>
					</div>
				</div>
				<!--标题 结束-->
				
				<!--内容 开始-->
				<div class="fr-goods-area">
					<!--左侧排版 开始-->
					<div class="goods-left">
						<a href="<?php echo __ROOT__?>/H_L_i_80.html" class="a-up"><img src="<?php echo __PUBLIC__?>/Home/images/dambqgayde_183x213.jpg" class="fade-in"/></a>
						<?php foreach ($brandData2 as $vv){?>
						<a href="<?php echo __ROOT__?>/H_L_i_80.html" class="a-btm" title="<?php echo $vv['bname']?>">
							<img src="<?php echo __ROOT__?>/<?php echo $vv['logo']?>" class="fade-in"/>
							<span class="hover_cover"></span>
						</a>
						<?php }?>
						
					</div>
					<!--左侧排版 结束-->
					
					<!--右侧排版开始-->
					<div class="goods-right">
						<div class="g-pic"><a href="<?php echo __ROOT__?>/H_L_i_1.html"><img src="<?php echo __PUBLIC__?>/Home/images/ambqhayde_305x500.jpg" alt="" /></a></div>
						<div class="c-goods">
							<div class="goods-list">
							
								<?php foreach ($goodsData3 as $vvo){?>
								<a href="<?php echo __ROOT__?>/H_L_i_<?php echo $vvo['category_cid']?>.html" class="goods-item">
									<img src="<?php echo __ROOT__?>/<?php echo $vvo['cate_img']?>" class="fade-in"/>
									<span class="goods-name"> 
										<span class="gd-name-ch"><?php echo $vvo['cate_name']?></span> 
									</span>
								</a>
								<?php }?>
								
							</div>
						</div>
					</div>
					<!--右侧排版结束-->
				</div>
				<!--内容结束-->
			</div>
			<!--第四层 结束-->
			
			
		</div>
		<!--楼层 结束-->
		
	<!--载入尾部-->
	<!--底部公共部分   开始-->
		<!--底部 开始-->
		<div class="floor-area clearfloat" style="margin-bottom: 10px;">
			<div class="floor">
				<div class="foot-info">
		            <a class="info-logo" href="#"></a>
		            <div class="info-text">
		                <p>站点名称：<a href="" target="_blank"><?php echo C('webset.webname')?></a></p>
		               	<p class="mgjhostname" title="guomai31072"><?php echo C('webset.webdes')?></p>
		            </div>
    			</div>
    			
				<div class="foot_link">
		            <dl class="link_company">
		                <dt>友情链接</dt>
		                <?php foreach ($linkData as $v){?>
		                <dd><a href="<?php echo $v['url']?>" target="_blank"><?php echo $v['lname']?></a></dd>
		                <?php }?>
		            </dl>
		          
        		</div>
			</div>
		<!-- 	<?php if( 'Index'=='Index' && 'index'=='index' ){?>
                
			<div class="w-links clearfloat">
        			<ul>
	            		<li>友情链接: </li>
	                    <li><a target="_blank" href="#">淘粉吧</a></li>
	                    <li><a target="_blank" href="#">蘑菇街团购网</a></li>
	                    <li><a target="_blank" href="#">蘑菇街女装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街男装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街鞋子</a></li>
	                    <li><a target="_blank" href="#">蘑菇街包包</a></li>
	                    <li><a target="_blank" href="#">蘑菇街家居</a></li>
	                    <li><a target="_blank" href="#">家具网</a></li>
	                    <li><a target="_blank" href="#">时尚品牌网</a></li>
	                    <li><a target="_blank" href="#">装修</a></li>
	                    <li><a target="_blank" href="#">蘑菇街母婴</a></li>
                	</ul>
    		</div>
    		
               <?php }?> -->
    		
		</div>
		<!--底部 结束-->
		<!--底部公共部分   结束-->
		
		<?php if( 'Index'=='Index' && 'index'=='index' ){?>
                
		<!--首页头部 弹出搜索框 开始-->
		<div class="sticky-search-container">
			<div class="fix-warp">
				<a href="#" class="logo" title="蘑菇街|我的买手街">蘑菇街|我的买手街</a>
				<div class="nav_search_form">
					<div class="search_inner_box">
						<div class="selectbox">
							<span class="selected">搜商品</span>
						</div>
						<form action="H_S.html" method="get" id="top_nav_form">
                            <input class="fx-txt" value="<?php echo isset($_GET['words'])?$_GET['words']:'裙子'?>"  type="text" name="words">
							<input value="搜  索" class="fx_btn"  type="submit">
            			</form>
					</div>
				</div>
			</div>
		</div>
		<!--首页头部 弹出搜索框 结束-->
		
               <?php }?>
		
		<!--用户信息除外-->
		<?php if( 'Index'!='Userinfo' && 'Index'!='Cart' && 'index'!='userinfo' ){?>
                
		<!--右侧购物车 回到顶部 相对定位 开始-->
		<div class="mgj_rightbar">
			<!--用户头像-->
			<div class="sidebar-item mgj-my-avatar">
				<a href="javascript:;" id="mg-userFace" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>">
					<?php if(isset($_SESSION['uid'])){?>
                
					<div class="img">
						<img src="<?php echo __ROOT__?>/<?php echo $face?>"  height="20" width="20">
					</div>
					<?php }else{?>
					<div class="img">
						<img src="<?php echo __PUBLIC__?>/Home/images/02.jpg_48x48.jpg" height="20" width="20">
					</div>
					
               <?php }?>
				</a>
			</div>
			<!--购物车-->
			<div class="sidebar-item mgj-my-cart" id="mgj-my-cart">
        		<a data-ptp-idx="2" target="_blank" href="<?php echo __ROOT__?>/H_C_i.html">
            		<i class="s-icon"></i>
            		<div class="s-txt">购物车</div>
            		<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
            		<div class="num floorNum"><?php echo $_SESSION['tatol']?></div>
            		
               <?php }?>
        		</a>
        		
        		<!--隐藏购物车div 开始-->
        		<div class="cart-hide-area hidden" id="cart-hide-area">
        			<p class="p1">
        				<span class="mac-success-txt module-cart-icons">已将商品添加到购物车</span>
        			</p>
        			<p><a href="<?php echo __ROOT__?>/H_C_i.html" class="mac-go-cart module-cart-icons">去购物车结算</a></p>
        			<a href="javascript:;" class="J_Close fix-close-btn">关闭</a>
        			<span class="sanxiao"></span>
        		</div>
        		
        		<!--隐藏购物车div 结束-->
    		</div>
    		<!--回到顶部-->
    		<div class="sideBottom">
       			<div class="sidebar-item mgj-back2top">
		            <a rel="nofollow" href="javascript:;">
		                <i class="s-icon"></i>
		            </a>
		        </div>
    		</div>
		</div>
		<!--右侧购物车 回到顶部 相对定位 结束-->
		
               <?php }?>
		
	</body>
<!-- 载入JS区域 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/jquery-1.8.3.min.js" ></script>

<!-- 载入HDjs -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>
<!-- 载入城市联动 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/city.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/formValidate.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/index.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/common.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/slide.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/goods.js"></script>




<!-- 载入Uploadify上传插件 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/method.js"></script>

</html>

	