<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>蘑菇街-个人中心-我的订单</title>
		<!--载入头部-->
				<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/common.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/floor.css"/>
		
		<!-- 载入HDjs样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css"/>
		<!-- 首页样式只有首页有 -->
		<?php if( 'Userinfo'=='Index' && 'myIndent'=='index' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/index.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Userinfo' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/userinfo.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Lists' ){?>
                
		<!-- 列表页样式  -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/lists.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Details' ){?>
                
		<!-- 详情页 样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/details.css"/>
		
               <?php }?>
		
		<!-- 购物车样式 -->
		<?php if( 'Userinfo'=='Cart' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/cart.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Userinfo' && 'myIndent'=='orderdetail' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/indent.css"/>
		
               <?php }?>
		
		<!-- 搜索页样式 -->
		<?php if( 'Userinfo'=='Search' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/seek.css" />
		
               <?php }?>
		
		<!-- 载入上传Uploadify样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">
		
		<script type="text/javascript">
				var Public = "<?php echo __PUBLIC__?>";
				var Root = "<?php echo __ROOT__?>";
				var sname = "<?php echo session_name();?>";
				var sid = "<?php echo session_id();?>";
				var userFaceUrl = "<?php echo U('Userinfo/face')?>";
				var payValueUrl = "<?php echo U('Common/payValue')?>";
		</script>
		
		
	</head>
	<body>
		<!-- HD模态框  开始 -->
		<div id="hd-modal-bg" style="opacity:0.7;filter:alpha(opacity=7);display:none;">背景遮罩</div>
		<div id="hd-modal" style="width:300px;height:180px;margin-left:-180px; display:none;">        
				<div class="hd-modal-wrap" style="height:180px">            
						 <div class="hd-modal-header">                
							<span class="hd-modal-title">余额充值</span>               
						 	<span class="hd-modal-close hd-clone-modal"></span>            
						  </div>            
				 		  <div class="hd-modal-message">
				 		  		<textarea name="pay" rows="4" id="text-area"  style="width:280px;" class="text-pay-value"></textarea>
				 		  </div>           
						  <div class="hd-modal-btn">                
							  <a class="hd-btn hd-btn-success hd-success-modal" href="javascript:;">确认充值</a>&nbsp;                
							  <a class="hd-btn hd-clone-modal" href="javascript:;">关闭</a>           
						   </div>        
				</div>    
		</div>
		<!-- HD模态框  结束 -->
		
		<!--公共头部样式 开始-->
		<!--头部 开始-->
		<div class="header-area">
			<div class="header">
				<a href="<?php echo __ROOT__?>" class="home">蘑菇街首页</a>
				<ul class="header-top">
					<!--用户名登录后 的div-->
					<?php if(isset($_SESSION['uid'])?$_SESSION['uid']:0){?>
                
					<li class="t1 has_icon user_meta" id="user_meta">
	                    <a href="javascript:;">很纯很暧昧777</a>
	                    <a href="" target="_blank"><span class="user-level user-level0">&nbsp;</span></a>
	                    <i class="icon_delta"></i>
	                    <ol class="ext_mode" id="menu_personal" style="display: none;">
	                        <li class="s2"><a target="_blank" href="H_U.html">个人设置</a></li>
	                        <li class="s2"><a target="_blank" href="H_U.html">账号绑定</a></li>
	                        <li class="s2"><a rel="nofollow" href="H_logout.html">退出</a></li>
	                    </ol>
                	</li>
                	<!--用户名登录后 的div 结束-->
                	<?php }else{?>
                	<!--没登录状态-->
					<li class="t1"><a href="H_reg.html">注册</a></li>
					<li class="t1"><a href="H_log.html">登录</a></li>
					<!--没登录状态 结束-->
					
               <?php }?>
					<li class="t1 myorder t1-line" id="J-order" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>"><a href="javascript:;">我的订单</a></li>
					<li class="t1 myorder t1-cate" id="t1-cate">
						<a href="H_C_i.html">购物车
							<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
							<span class="floorNum" style="color: #f36;padding: 0px;font-weight: 400;"><?php echo $_SESSION['tatol']?></span>
							<span>件</span>
							
               <?php }?>
						</a>
						<!--购物车隐藏盒子 开始-->
						
						<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
						<!--有商品的时候-->
						<div class="cate-hide cate-info" style="display: none;">
							<ul>
								<?php foreach ($_SESSION['goods'] as $v){?>
	                            <li>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="imgbox">
										<img src="<?php echo __ROOT__?>/<?php echo $v['pic']?>"  width="45" height="45">
									</a>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="title"><?php echo $v['gname']?></a>
									<span class="info">
											<?php foreach ($v['options'] as $gtname=>$vo){?>
									    	<span><?php echo $gtname?>：<?php echo $vo?></span>
									    	<?php }?>
									</span>
									<span class="price">￥<?php echo $v['shopprice']?></span>
									<a href="javascript:;" class="del delete" mgprice="<?php echo $v['price']?>" glid="<?php echo $v['glid']?>">删除</a>
								</li>
								<?php }?>
								
							</ul>
							<div class="subbox">
                    			<div class="fr">
                				<a href="H_C_i.html" target="_blank" class="goel">查看购物车</a>
            					</div>
        					</div>
						</div>
						<!--购物车隐藏盒子 结束-->
						<?php }else{?>
						<!--没有商品的时候-->
						<div class="cate-hide empty_cart" style="display: none;">
							购物车里没有商品！
						</div>
						<!--没有商品的时候 结束-->
						
               <?php }?>
					</li>
				</ul>
				
			</div>
		</div>
		<!--头部 结束-->
		
		<!--搜索区域   开始-->
		<div class="search-warp">
			<div class="search-area">
			<div class="logo">
				<a href="<?php echo __ROOT__?>" title="蘑菇街首页"></a>
			</div>
			
			<!--搜索框 区域-->
			<div class="search">
				<div class="search-box">
					<div class="selectbox">
                		<span class="selected">搜商品</span>
               		</div>
               		<!--form 表单 开始-->
               		<form action="H_S.html" method="get" id="sea-form">
               			<input type="text" value="<?php echo isset($_GET['words'])?$_GET['words']:'上衣'?>" class="ts-txt" name="words"/>
               			<input type="submit" value="搜  索" class="ts-btn"/>
               		</form>
               		<!--form 表单 结束-->
               	</div>
               	
               	<!--栏目列表  开始-->
               	<div class="cate-list">
               		<?php foreach ($cateData_cache as $v){?>
               		<a href="H_S.html?words=<?php echo $v['cname']?>"><?php echo $v['cname']?></a>
               		<?php }?>
               	</div>
               	<!--栏目列表 结束-->
				
			</div>
			<!--搜索框 结束-->
			
			<!--二维码 开始-->
	       		<!-- <div class="e-qrcode">
	                <img src="<?php echo __PUBLIC__?>/Home/images/ecode.png" alt="蘑菇街客户端下载" height="70" width="70">
	                <p>蘑菇街客户端</p>
	            </div> -->
       		<!--结束-->
		</div>
		</div>
		<!--搜索区域   结束-->
		
		<?php if( 'Userinfo'=='Index' && 'myIndent'=='index' ){?>
                
		<!--菜单列表 开始-->
		<div class="menu-area">
			<div class="menu">
				<ul class="menu-list">
					<li class="home">全部商品</li>
									<?php
					$model = new \Common\Model\Category();
					$data = $model->where('pid=0 AND is_show=1')->limit(8)->get();
					foreach($data as $field):
					//列表页地址
					$field['url'] =__ROOT__ .  "/H_L_i_". $field['cid'] . ".html";
				?>
					
					<li><a href="<?php echo $field['url']?>"><?php echo $field['cname']?></a></li>
					
				<?php endforeach;?>
				</ul>
			</div>
		</div>
		<!--菜单列表 结束-->
		
               <?php }?>
		
		<!--公共头部样式 结束-->
		<script type="text/javascript">
					var delOrderUrl = "<?php echo U('Common/delOrder')?>";
					var confirmOrderUrl = "<?php echo U('Common/confirmOrder')?>";
		</script>
		<!--下滑线2px-->
		<div class="overstriking"></div>
		
		<!--公共头部样式 结束-->
		
		<!--主体内容 开始-->
		<div class="fm1200 clearfloat">
			<div class="meu-warp">
				<!--用户信息 左侧 开始-->
				<div class="mu-nav-wrap">
<div class="nav-info">
<!--头像-->
<div class="nav-info-avatar">
<a href="H_U.html"><div class="mu_nav_info_avatar_mk"></div></a>

<?php if($face){?>
                
<img src="<?php echo __ROOT__?>/<?php echo $face?>" height="100" width="100">
<?php }else{?>
<img src="<?php echo __PUBLIC__?>/Home/images/default_100x100.jpg" height="100" width="100">

               <?php }?>

</div>

<!--用户名-->
				<?php
					$userModel = new \Common\Model\User();
					$nickname = $userModel->where(array('uid'=>$_SESSION['uid']))->pluck('nickname');
				?>
			
<a href="H_U.html"><p class="mu_nav_info_uname"><?php echo $nickname?></p></a>

<a style="width:45px;" class="mu_nav_info_ulot">
<span id="vip_level" class="vip_level0" style="float:left;width: 45px; display: inline-block; height: 14px; margin-top: 3px;"></span>
</a>
</div>

<!--我的订单-->
<dl class="mu-nav">
<dt>我的订单</dt>
<dd <?php if($status==0 && 'myIndent'=='myIndent' ){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_0.html" <?php if($status==0 && 'myIndent'=='myIndent' ){?>
                class="red"
               <?php }?>>全部订单<?php if($status==0 && 'myIndent'=='myIndent' ){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?></a>
</dd>
<dd <?php if($status==1){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_1.html" <?php if($status==1){?>
                class="red"
               <?php }?>>待付款 <?php if($status==1){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?></a>
</dd>
<dd <?php if($status==2){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_2.html" <?php if($status==2){?>
                class="red"
               <?php }?>>待发货<?php if($status==2){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?> </a>
</dd>
<dd <?php if($status==3){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_3.html" <?php if($status==3){?>
                class="red"
               <?php }?>>待收货<?php if($status==3){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?> </a>
</dd>
<dd <?php if($status==4){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_4.html" <?php if($status==4){?>
                class="red"
               <?php }?>>已完成<?php if($status==4){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?> </a>
</dd>

</dl>
<!--地址管理-->
<dl class="mu-nav">
<dt><a href="H_U_site_0.html">地址管理</a></dt>
</dl>

<dl class="mu-nav">
<dt><a href="javascript:;" class="hd-success-area">我的钱包</a></dt>
</dl>
<!--账号设置-->
<dl class="mu-nav">
<dt>帐号设置</dt>
<dd <?php if( 'Userinfo'=='Userinfo' && 'myIndent'=='userinfo' ){?>
                class="cur"
               <?php }?>> <a href="H_U.html" <?php if( 'Userinfo'=='Userinfo' && 'myIndent'=='userinfo' ){?>
                class="red"
               <?php }?>>基本信息</a> </dd>
<dd <?php if( 'Userinfo'=='Userinfo' && 'myIndent'=='editFace' ){?>
                class="cur"
               <?php }?>> <a href="H_U_face.html" <?php if( 'Userinfo'=='Userinfo' && 'myIndent'=='editFace' ){?>
                class="red"
               <?php }?>>修改头像</a> </dd>
</dl>
</div>

				<!--用户信息 左侧 结束-->
				
				<!--用户右侧个人信息  开始-->
				<div class="mu_right_wrap">
					<!--我的订单 标题栏-->
					<div class="order-title">
					    <ul class="order-title-column">
					        <li class="goods">商品</li>
					        <li class="price">单价(元)</li>
					        <li class="quantity">数量</li>
					      	<li class="total">实付款(元)</li>
					        <li class="status">交易状态</li>
					        <li class="other">操作</li>
					    </ul>
					</div>
					<!--订单详情 开始-->
					<?php if($orderData){?>
                
					<div class="order-list clearfloat">
						<div class="order-section">
							<!--循环层 开始-->
							<?php foreach ($orderData as $v){?>
							<table class="order-table" style="margin-bottom:20px; ">
								<tbody>
									<tr class="order-table-header">
										<td colspan="6">
											<div class="order-info">
												<span>订单编号：<span class="order_num"><?php echo $v['number']?></span></span>
												<span>成交时间：<span class="order_num"><?php echo date('Y-m-d H:i:s',$v['time'])?></span></span>
											</div>
										</td>
									</tr>
									
									<tr class="order-table-item">
										<td class="goods">
											<a class="pic" href="#" title="查看宝贝详情" hidefocus="true" target="_blank">
												<img src="<?php echo __ROOT__?>/<?php echo $v['pic']?>" alt="查看宝贝详情" width="70" height="70">
											</a>
											<div class="desc">
												<p>
													<a href="H_D_i_<?php echo $v['goods_gid']?>.html" target="_blank"><?php echo msubstr($v['gname'],0,14)?></a>
													<!--订单快照-->
													<a class="snapshot" href="H_D_i_<?php echo $v['goods_gid']?>.html" target="_blank">[交易快照]</a>
												</p>
												<?php foreach ($v['options'] as $gtname=>$vo){?>
												<p><?php echo $gtname?> ：<?php echo $vo?></p>
												<?php }?>
											</div>
										</td>
										<td class="price" style="text-align:center;">
            								<p class="price-old">￥<?php echo $v['marketprice']?></p>
                                    		<p>￥<?php echo $v['shopprice']?></p>
										</td>
										<td class="quantity"><?php echo $v['quantity']?></td>
										<td class="total" rowspan="1">
						                    <ul>
												<li>
													<p class="total-price">￥<?php echo $v['subtotal']?></p>
													<p>(包邮)</p>
													<p></p>
												</li>
											</ul>
										</td>
										<td class="status" rowspan="1">
											<?php if($v['status']==1){?>
                
						                    <p class="">待付款</p>
						                    <?php }else if($v['status']==2){?>
						                     <p class="">待发货</p>
						                     <?php }else if($v['status']==3){?>
						                     <p class="">已发货</p>
						                     <?php }else{?>
						                      <p class="">已完成</p>
						                     
               <?php }?>
						                    <a class="order-link" target="_blank" href="H_U_odd_<?php echo $v['oid']?>.html">订单详情</a>
										</td>
										<td class="other" rowspan="1">
                                           <ul>
												<li>
													<?php if($v['status']==1){?>
                
													<a class="order-link order-remove" href="javascript:;" oid="<?php echo $v['oid']?>" glid="<?php echo $v['glid']?>">取消订单</a>
													
               <?php }?>
													<?php if($v['status']==2){?>
                
													<a class="order-link order-remind" href="javascript:;" >提醒卖家发货</a>
													
               <?php }?>
													<?php if($v['status']==4){?>
                
													<a class="order-linkremind" href="javascript:;">完成交易</a>
													
               <?php }?>
													<?php if($v['status']==3){?>
                
													<a class="order-link order-confirm" href="javascript:;" oid="<?php echo $v['oid']?>">确认收货</a>
													
               <?php }?>
													
												</li>
											</ul>
										</td>
									</tr>
									<tr style="height: 56px;">
            							<td  style="font-weight: 700;" <?php if($v['status']!=1){?>
                colspan="3"<?php }else{?>colspan="4"
               <?php }?>>
            									<span>总计：</span>
            									<span style="color: #f36;">￥<?php echo $v['subtotal']?> </span>
            							</td>
            							<?php if($v['status']!=1){?>
                
            							<td  style="font-weight: 700;">
            									<?php if($v['paymethod']==1){?>
                
            									<span>付款方式:</span>
            									<span>货到付款</span>
            									<?php }else if($v['paymethod']==2){?>
            									<span>付款方式:</span>
            									<span>余额支付</span>
            									
               <?php }?>
            							</td>
            							
               <?php }?>
										<?php if($v['status']==1){?>
                
										<td style="text-align: center;color: #666;font-weight: 700;">等待付款</td>
										<?php }else if($v['status']==2){?>
										<td style="text-align: center;color: #666;font-weight: 700;">等待卖家发货</td>
										 <?php }else if($v['status']==3){?>
										 <td style="text-align: center;color: #666;font-weight: 700;">卖家已发货</td>
										 <?php }else{?>
										  <td style="text-align: center;color: #666;font-weight: 700;">订单交易完成</td>
										  
               <?php }?>
										<td style="text-align: center;">
											<?php if($v['status']==1){?>
                
											<a href="H_C_ali_<?php echo $v['oid']?>.html" style="display: inline-block;background: #f36;color: #fff;padding: 3px 9px;min-width: 52px;border: 1px solid #f36;border-radius: 3px;font-weight: 700;">去付款</a>
											<?php }else if($v['status']==2 || $v['status']==3){?>
											<a href="javascript:;" style="display: inline-block;background: #599EF4;;color: #fff;padding: 3px 9px;min-width: 52px;border: 1px solid #2982F1;border-radius: 3px;font-weight: 700;">已付款</a>
											<?php }else{?>
											<a href="javascript:;" style="display: inline-block;background: #9C3CFF;color: #fff;padding: 3px 9px;min-width: 52px;border: 1px solid #9C3CFF;border-radius: 3px;font-weight: 700;">交易完成</a>
											
               <?php }?>
										</td>
									</tr>
								</tbody>
							</table>
							<?php }?>
							<!--循环层 结束-->
						</div>
					</div>
					
					<!--订单详情 结束-->
					<?php }else{?>
					<!--没有订单的时候 开始-->
					<div class="order-list-empty clearfloat">
        				<div class="empty-icon fl"></div>
						<div class="empty-content fl">
				            <h5 class="empty-title">哎呀，此状态下没有对应的订单！</h5>
				            <ul class="empty-list">
				                <li>去 <a href="H_S.html?words=上衣">服饰</a> 看看大家都喜欢的商品</li>
				             </ul>
				        </div>
				    </div>
				    <div class="pagination"></div>
					<!--没有订单的时候 结束-->
					
               <?php }?>
				</div>
				<!--用户右侧个人信息  结束-->
			</div>
		</div>
		<!--主体内容 结束-->
		
		
		<!--底部公共部分   开始-->
		<!--载入尾部-->
		<!--底部公共部分   开始-->
		<!--底部 开始-->
		<div class="floor-area clearfloat" style="margin-bottom: 10px;">
			<div class="floor">
				<div class="foot-info">
		            <a class="info-logo" href="#"></a>
		            <div class="info-text">
		                <p>站点名称：<a href="" target="_blank"><?php echo C('webset.webname')?></a></p>
		               	<p class="mgjhostname" title="guomai31072"><?php echo C('webset.webdes')?></p>
		            </div>
    			</div>
    			
				<div class="foot_link">
		            <dl class="link_company">
		                <dt>友情链接</dt>
		                <?php foreach ($linkData as $v){?>
		                <dd><a href="<?php echo $v['url']?>" target="_blank"><?php echo $v['lname']?></a></dd>
		                <?php }?>
		            </dl>
		          
        		</div>
			</div>
		<!-- 	<?php if( 'Userinfo'=='Index' && 'myIndent'=='index' ){?>
                
			<div class="w-links clearfloat">
        			<ul>
	            		<li>友情链接: </li>
	                    <li><a target="_blank" href="#">淘粉吧</a></li>
	                    <li><a target="_blank" href="#">蘑菇街团购网</a></li>
	                    <li><a target="_blank" href="#">蘑菇街女装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街男装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街鞋子</a></li>
	                    <li><a target="_blank" href="#">蘑菇街包包</a></li>
	                    <li><a target="_blank" href="#">蘑菇街家居</a></li>
	                    <li><a target="_blank" href="#">家具网</a></li>
	                    <li><a target="_blank" href="#">时尚品牌网</a></li>
	                    <li><a target="_blank" href="#">装修</a></li>
	                    <li><a target="_blank" href="#">蘑菇街母婴</a></li>
                	</ul>
    		</div>
    		
               <?php }?> -->
    		
		</div>
		<!--底部 结束-->
		<!--底部公共部分   结束-->
		
		<?php if( 'Userinfo'=='Index' && 'myIndent'=='index' ){?>
                
		<!--首页头部 弹出搜索框 开始-->
		<div class="sticky-search-container">
			<div class="fix-warp">
				<a href="#" class="logo" title="蘑菇街|我的买手街">蘑菇街|我的买手街</a>
				<div class="nav_search_form">
					<div class="search_inner_box">
						<div class="selectbox">
							<span class="selected">搜商品</span>
						</div>
						<form action="H_S.html" method="get" id="top_nav_form">
                            <input class="fx-txt" value="<?php echo isset($_GET['words'])?$_GET['words']:'裙子'?>"  type="text" name="words">
							<input value="搜  索" class="fx_btn"  type="submit">
            			</form>
					</div>
				</div>
			</div>
		</div>
		<!--首页头部 弹出搜索框 结束-->
		
               <?php }?>
		
		<!--用户信息除外-->
		<?php if( 'Userinfo'!='Userinfo' && 'Userinfo'!='Cart' && 'myIndent'!='userinfo' ){?>
                
		<!--右侧购物车 回到顶部 相对定位 开始-->
		<div class="mgj_rightbar">
			<!--用户头像-->
			<div class="sidebar-item mgj-my-avatar">
				<a href="javascript:;" id="mg-userFace" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>">
					<?php if(isset($_SESSION['uid'])){?>
                
					<div class="img">
						<img src="<?php echo __ROOT__?>/<?php echo $face?>"  height="20" width="20">
					</div>
					<?php }else{?>
					<div class="img">
						<img src="<?php echo __PUBLIC__?>/Home/images/02.jpg_48x48.jpg" height="20" width="20">
					</div>
					
               <?php }?>
				</a>
			</div>
			<!--购物车-->
			<div class="sidebar-item mgj-my-cart" id="mgj-my-cart">
        		<a data-ptp-idx="2" target="_blank" href="<?php echo __ROOT__?>/H_C_i.html">
            		<i class="s-icon"></i>
            		<div class="s-txt">购物车</div>
            		<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
            		<div class="num floorNum"><?php echo $_SESSION['tatol']?></div>
            		
               <?php }?>
        		</a>
        		
        		<!--隐藏购物车div 开始-->
        		<div class="cart-hide-area hidden" id="cart-hide-area">
        			<p class="p1">
        				<span class="mac-success-txt module-cart-icons">已将商品添加到购物车</span>
        			</p>
        			<p><a href="<?php echo __ROOT__?>/H_C_i.html" class="mac-go-cart module-cart-icons">去购物车结算</a></p>
        			<a href="javascript:;" class="J_Close fix-close-btn">关闭</a>
        			<span class="sanxiao"></span>
        		</div>
        		
        		<!--隐藏购物车div 结束-->
    		</div>
    		<!--回到顶部-->
    		<div class="sideBottom">
       			<div class="sidebar-item mgj-back2top">
		            <a rel="nofollow" href="javascript:;">
		                <i class="s-icon"></i>
		            </a>
		        </div>
    		</div>
		</div>
		<!--右侧购物车 回到顶部 相对定位 结束-->
		
               <?php }?>
		
	</body>
<!-- 载入JS区域 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/jquery-1.8.3.min.js" ></script>

<!-- 载入HDjs -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>
<!-- 载入城市联动 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/city.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/formValidate.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/index.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/common.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/slide.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/goods.js"></script>




<!-- 载入Uploadify上传插件 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/method.js"></script>

</html>

	