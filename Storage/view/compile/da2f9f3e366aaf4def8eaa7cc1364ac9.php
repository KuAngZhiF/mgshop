<!--载入头部-->
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>后台管理系统</title>
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/style.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/node.css" />


<link rel="stylesheet" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css">

<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/uniform.default.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/jquery.fancybox.css">

<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">

<!-- 载入文本编译器 -->
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript">
	var Public = "<?php echo __PUBLIC__?>";
	var Root = "<?php echo __ROOT__?>";
	var picUploadUrl = "<?php echo U('Lists/picUploads')?>";
	var MiniUploadUrl = "<?php echo U('Lists/MiniUploads')?>";
	var sname = "<?php echo session_name();?>";
	var sid = "<?php echo session_id();?>";
	var FaceUrl = "<?php echo U('System/face')?>";
	var imgsGoodsUrl = "<?php echo U('Goods/imgsGoods')?>";
	var CateMiniUrl = "<?php echo U('Lists/CateMiniUp')?>";
	var CateMaxUrl = "<?php echo U('Lists/CateMaxUpOne')?>";
	var CateMaxtwoUrl = "<?php echo U('Lists/CateMaxUpTwo')?>";
	var CateDelUrl = "<?php echo U('Lists/CateAjaxDel')?>";
	var cateImgDelUrl = "<?php echo U('Goods/cateImgDel')?>";
</script>

</head>
<body>
<div class="topbar clearfix">
	<div class="container-fluid">
		<a href="#" class='company'>MgShop后台管理系统</a>
		<ul class='mini'>
			<li>
				<a href="/" target="_blank">
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/gear.png" alt="">
					查看网站
				</a>
			</li>		
					
			<li>
				<a href="<?php echo U('System/systemSet')?>">
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/gear.png" alt="">
					网站设置
				</a>
			</li>		
			<li>
				<a href="<?php echo U('Login/loginOut')?>" id='loginOut'>
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/control-power.png" alt="">
					退出系统
				</a>
			</li>
		</ul>
	</div>
</div>
<!--面包屑-->
<div class="breadcrumbs">
	<div class="container-fluid">
		<ul class="bread pull-left">
			<li>
				<a href="dashboard.html"><i class="icon-home icon-white"></i></a>
			</li>
			<li>
				<a href="dashboard.html">
					
				</a>
			</li>
		</ul>

	</div>
</div>
<!--面包屑结束-->

<script type="text/javascript">
	var getAttrUrl = "<?php echo U('Lists/getAttr')?>";
	var getNormsUrl = "<?php echo U('Lists/getNorms')?>";
	var delPic = "<?php echo U('Lists/delPic')?>";
	//var delMini = "<?php echo U('Lists/delMini')?>";
</script>

<style type="text/css">
		/*去掉uploadify上传按钮的边框*/
	    .uploadify-button {
	        background-color: transparent;
	        border: none;
	        padding: 0;
	    }
	    .uploadify-button:hover {
	        background-color: transparent;
    	}
 </style> 


<div class="main">
	<div class="container-fluid">
	
	<!--左边的导航-->
	<div class="navi">
		<ul class='main-nav'>
			<li <?php if( 'Goods'=='Index'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-home icon-white"></i></div>
					控制面板
					<?php if( 'Goods'=='Index' ){?>
                
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-up-white.png" alt="">
					<?php }else{?>
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
					
               <?php }?>					
				</a>
				
				<ul <?php if( 'Goods'=='Index'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Index' && 'addGoods'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Index/index')?>">
							后台首页
						</a>
					</li>
				</ul>
			</li>
			<li <?php if( 'Goods'=='Goods'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					商品管理
					<?php if( 'Goods'=='Goods' ){?>
                
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-up-white.png" alt="">
					<?php }else{?>
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
					
               <?php }?>
				</a>
				<ul <?php if( 'Goods'=='Goods'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Goods' && 'addGoods'=='listType' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listType')?>">
							类型管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'addGoods'=='ListCategory' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/ListCategory')?>">
							分类管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'addGoods'=='CategoryImg' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/CategoryImg')?>">
							分类图片管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'addGoods'=='cateImgList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/cateImgList')?>">
							栏目图片管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'addGoods'=='listBrand' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listBrand')?>">
							品牌管理
						</a>
					</li>
					
					<li <?php if( 'Goods'=='Goods' && 'addGoods'=='listGoods' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listGoods')?>">
							商品管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'addGoods'=='recycle' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/recycle')?>">
							回收站
						</a>
					</li>
				</ul>
			</li>
			
			<li <?php if( 'Goods'=='Order'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					订单管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Order'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Order' && 'addGoods'=='orderList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Order/orderList')?>">
							订单列表
						</a>
					</li>
					
				</ul>
			</li>
			
			<li <?php if( 'Goods'=='Pay'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					充值管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Pay'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Pay' && 'addGoods'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Pay/index')?>">
							充值管理
						</a>
					</li>
					
				</ul>
			</li>

			<!-- <li >
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-book icon-white"></i></div>
					评论管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul class='collapsed-nav closed'>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							评论列表
						</a>
					</li>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							检索评论
						</a>
					</li>					
				</ul>
			</li> -->
			


			<li <?php if( 'Goods'=='System'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-exclamation-sign icon-white"></i></div>
					系统管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='System'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='System' && 'addGoods'=='linkList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/linkList')?>">
							友情链接
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'addGoods'=='UserFace' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/UserFace')?>">
							修改头像
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'addGoods'=='editUser' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/editUser')?>">
							修改密码
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'addGoods'=='systemSet' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/systemSet')?>">
							网站设置
						</a>
					</li>
				</ul>
			</li>
		
			

			 <li <?php if( 'Goods'=='Rbac'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-tasks icon-white"></i></div>
					RBAC权限管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Rbac'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Rbac' && 'addGoods'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/index')?>">
							用户列表
						</a>
					</li>
					
					<li <?php if( 'Goods'=='Rbac' && 'addGoods'=='Rolelist' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/Rolelist')?>">
							角色列表
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'addGoods'=='addRole' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/addRole')?>">
							添加角色
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'addGoods'=='nodelist' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/nodelist')?>">
							节点列表
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'addGoods'=='addNode' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/addNode')?>">
							添加节点
						</a>
					</li>
				</ul>
			</li>
	

			
		</ul>
	</div>
	<!--左边的导航 结束-->

	<div class="content">
			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-head">
							<h3 ><button class="btn btn-info">添加商品</button></h3>
							<div style="float:right;margin:5px; cursor:pointer;" onclick='location.href="<?php echo U('listGoods')?>"'>返回到上一级&nbsp<img src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/16/refresh.png"  title="返回到上一级" ></div>
						</div>
						<div class="box-content">
							<form action="" class="form-horizontal" method="post" id="addGoodsForm">
									<div class="alert alert-info alert-block">
  											<h4 class="alert-heading">基本信息</h4>
  									</div>
  									
  									<div class="control-group">
										<label for="select" class="control-label">所属分类</label>
										<div class="controls">
											<select name="category_cid" id="categorySelect" style="width: 224px;padding-left:5px;">
												<option value=""  tid="0">请选择...</option>
												<?php foreach ($cateData as $v){?>
												<option value="<?php echo $v['cid']?>"  tid="<?php echo $v['goods_type_tid']?>"><?php echo $v['_name']?></option>
												<?php }?>
											</select>
										</div>
									</div>
									
									<div class="control-group">
										<label for="select" class="control-label">所属品牌</label>
										<div class="controls">
											<select name="brand_bid"  style="width: 224px;padding-left:5px;">
												<option value="">请选择...</option>
												<?php foreach ($brandData as $v){?>
												<option value="<?php echo $v['bid']?>"><?php echo $v['bname']?></option>
												<?php }?>
											</select>
										</div>
									</div>
									
									
									<div class="control-group">
										<label for="select" class="control-label">所属商品类型</label>
										<div class="controls">
											<select name="goods_type_tid"  style="width: 224px;padding-left:5px;">
												<option value="">请选择...</option>
												<?php foreach ($TypeData as $v){?>
												<option value="<?php echo $v['tid']?>"><?php echo $v['tname']?></option>
												<?php }?>
											</select>
										</div>
									</div>
									
									
									<div class="control-group">
										<label for="goods_name" class="control-label">商品名称</label>
										<div class="controls">
											<input type="text" name="gname" class='input-square' value="">
										</div>
									</div>
									
									<div class="control-group">
										<label for="goods_sn" class="control-label">单位</label>
										<div class="controls">
											<input type="text" name="unit" id="goods_sn" class='input-square'  value="件">
										</div>
									</div>
									
									<div class="control-group">
										<label for="goods_sn" class="control-label">市场价</label>
										<div class="controls">
											<input type="text" name="marketprice"  class='input-square'  value="">
										</div>
									</div>
									
									<div class="control-group">
										<label for="goods_sn" class="control-label">商城价</label>
										<div class="controls">
											<input type="text" name="shopprice" class='input-square'  value="">
										</div>
									</div>
									<div class="control-group">
										<label for="goods_sn" class="control-label">商品库存</label>
										<div class="controls">
											<input type="text" name="repertory" class='input-square'  value="">
										</div>
									</div>
									
									<div class="alert alert-info alert-block">
  											<h4 class="alert-heading">商品属性</h4>
  									</div>
  									<!-- 商品属性  开始 -->
  									<div class="goods-attr" ></div>
									<!-- 商品属性  结束 -->
									
									<div class="alert alert-info alert-block">
  											<h4 class="alert-heading">商品规格</h4>
  									</div>
  									<!-- 商品属性  规格  开始-->
  									<div class="goods-norms"></div>
									<!-- 商品属性  规格 结束-->
									
									
									<div class="alert alert-info alert-block">
  											<h4 class="alert-heading">列表图</h4>
  									</div>
  									<div class="control-group">
										<label for="goods_sn" class="control-label">上传列表图</label>
										<div class="controls">
                                            <input type="hidden" name='pic' value='' id="pictrue"/>
                                            <input type="file" name='pictrue' id='pic'/>
										</div>
										<!-- 删除按钮 -->
										<div class="controls" id="pic-show" style="position: relative;padding-top:10px;">
                                            <img alt="" src=""  class="pic-img-show" style="width:100px;height:100px;">
                                            <a href="javascript:;" class="busyPic" style="display: block;position: absolute;top: -5px;left: 86px;" title="删除图片？"><img alt="" src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/16/busy.png"></a>
										</div>
									</div>
									
									
									<div class="alert alert-info alert-block" style="position: relative;">
  											<h4 class="alert-heading">商品详细</h4>
  											<a href="javascript:" style="position: absolute;top: 3px;right: 5px;" title="打开文本编译器" id="intro"><img alt="" src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/32/communication.png"></a>
  									</div>
									<div class="control-group goods-editor2" style="display:none;">
										<script id="editor" type="text/plain" style="width:100%;height:240px;" name="intro"></script>
										<script>
							    			var ue = UE.getEditor('editor');
										</script>
									</div>
									
									
									<div class="alert alert-info alert-block" style="position: relative;">
  											<h4 class="alert-heading">售后服务</h4>
  											<a href="javascript:;" style="position: absolute;top: 3px;right: 5px;" title="打开文本编译器" id="service"><img alt="" src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/32/communication.png"></a>
  									</div>
									<div class="control-group goods-editor1" style="display:none;">
										<script id="editor2" type="text/plain" style="width:100%;height:240px;" name="service"></script>
										<script>
							    			var ue2 = UE.getEditor('editor2');
										</script>
									</div>
									<div class="form-actions">
										<button class="btn btn-primary" type="submit">提交</button>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>

		
		</div>	
	</div>
</div>	
<!--载入尾部-->
<script src="<?php echo __PUBLIC__?>/Admin/js/jquery.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/common.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/goods.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/formValidate.js"></script>
<script src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>

<script src="<?php echo __PUBLIC__?>/Admin/js/bootstrap.min.js"></script>


<script src="<?php echo __PUBLIC__?>/Admin/js/admin.js"></script>
<!-- <script src="<?php echo __PUBLIC__?>/Admin/My97DatePicker/WdatePicker.js"></script> -->
<script src="<?php echo __PUBLIC__?>/Admin/js/jquery.fancybox.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/js/custom.js"></script>


</body>
</html>
