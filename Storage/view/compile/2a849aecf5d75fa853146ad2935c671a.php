<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>蘑菇街-个人中心-地址管理</title>
		<!--载入头部-->
				<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/common.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/floor.css"/>
		
		<!-- 载入HDjs样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css"/>
		<!-- 首页样式只有首页有 -->
		<?php if( 'Userinfo'=='Index' && 'site'=='index' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/index.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Userinfo' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/userinfo.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Lists' ){?>
                
		<!-- 列表页样式  -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/lists.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Details' ){?>
                
		<!-- 详情页 样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/details.css"/>
		
               <?php }?>
		
		<!-- 购物车样式 -->
		<?php if( 'Userinfo'=='Cart' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/cart.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Userinfo' && 'site'=='orderdetail' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/indent.css"/>
		
               <?php }?>
		
		<!-- 搜索页样式 -->
		<?php if( 'Userinfo'=='Search' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/seek.css" />
		
               <?php }?>
		
		<!-- 载入上传Uploadify样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">
		
		<script type="text/javascript">
				var Public = "<?php echo __PUBLIC__?>";
				var Root = "<?php echo __ROOT__?>";
				var sname = "<?php echo session_name();?>";
				var sid = "<?php echo session_id();?>";
				var userFaceUrl = "<?php echo U('Userinfo/face')?>";
				var payValueUrl = "<?php echo U('Common/payValue')?>";
		</script>
		
		
	</head>
	<body>
		<!-- HD模态框  开始 -->
		<div id="hd-modal-bg" style="opacity:0.7;filter:alpha(opacity=7);display:none;">背景遮罩</div>
		<div id="hd-modal" style="width:300px;height:180px;margin-left:-180px; display:none;">        
				<div class="hd-modal-wrap" style="height:180px">            
						 <div class="hd-modal-header">                
							<span class="hd-modal-title">余额充值</span>               
						 	<span class="hd-modal-close hd-clone-modal"></span>            
						  </div>            
				 		  <div class="hd-modal-message">
				 		  		<textarea name="pay" rows="4" id="text-area"  style="width:280px;" class="text-pay-value"></textarea>
				 		  </div>           
						  <div class="hd-modal-btn">                
							  <a class="hd-btn hd-btn-success hd-success-modal" href="javascript:;">确认充值</a>&nbsp;                
							  <a class="hd-btn hd-clone-modal" href="javascript:;">关闭</a>           
						   </div>        
				</div>    
		</div>
		<!-- HD模态框  结束 -->
		
		<!--公共头部样式 开始-->
		<!--头部 开始-->
		<div class="header-area">
			<div class="header">
				<a href="<?php echo __ROOT__?>" class="home">蘑菇街首页</a>
				<ul class="header-top">
					<!--用户名登录后 的div-->
					<?php if(isset($_SESSION['uid'])?$_SESSION['uid']:0){?>
                
					<li class="t1 has_icon user_meta" id="user_meta">
	                    <a href="javascript:;">很纯很暧昧777</a>
	                    <a href="" target="_blank"><span class="user-level user-level0">&nbsp;</span></a>
	                    <i class="icon_delta"></i>
	                    <ol class="ext_mode" id="menu_personal" style="display: none;">
	                        <li class="s2"><a target="_blank" href="H_U.html">个人设置</a></li>
	                        <li class="s2"><a target="_blank" href="H_U.html">账号绑定</a></li>
	                        <li class="s2"><a rel="nofollow" href="H_logout.html">退出</a></li>
	                    </ol>
                	</li>
                	<!--用户名登录后 的div 结束-->
                	<?php }else{?>
                	<!--没登录状态-->
					<li class="t1"><a href="H_reg.html">注册</a></li>
					<li class="t1"><a href="H_log.html">登录</a></li>
					<!--没登录状态 结束-->
					
               <?php }?>
					<li class="t1 myorder t1-line" id="J-order" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>"><a href="javascript:;">我的订单</a></li>
					<li class="t1 myorder t1-cate" id="t1-cate">
						<a href="H_C_i.html">购物车
							<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
							<span class="floorNum" style="color: #f36;padding: 0px;font-weight: 400;"><?php echo $_SESSION['tatol']?></span>
							<span>件</span>
							
               <?php }?>
						</a>
						<!--购物车隐藏盒子 开始-->
						
						<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
						<!--有商品的时候-->
						<div class="cate-hide cate-info" style="display: none;">
							<ul>
								<?php foreach ($_SESSION['goods'] as $v){?>
	                            <li>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="imgbox">
										<img src="<?php echo __ROOT__?>/<?php echo $v['pic']?>"  width="45" height="45">
									</a>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="title"><?php echo $v['gname']?></a>
									<span class="info">
											<?php foreach ($v['options'] as $gtname=>$vo){?>
									    	<span><?php echo $gtname?>：<?php echo $vo?></span>
									    	<?php }?>
									</span>
									<span class="price">￥<?php echo $v['shopprice']?></span>
									<a href="javascript:;" class="del delete" mgprice="<?php echo $v['price']?>" glid="<?php echo $v['glid']?>">删除</a>
								</li>
								<?php }?>
								
							</ul>
							<div class="subbox">
                    			<div class="fr">
                				<a href="H_C_i.html" target="_blank" class="goel">查看购物车</a>
            					</div>
        					</div>
						</div>
						<!--购物车隐藏盒子 结束-->
						<?php }else{?>
						<!--没有商品的时候-->
						<div class="cate-hide empty_cart" style="display: none;">
							购物车里没有商品！
						</div>
						<!--没有商品的时候 结束-->
						
               <?php }?>
					</li>
				</ul>
				
			</div>
		</div>
		<!--头部 结束-->
		
		<!--搜索区域   开始-->
		<div class="search-warp">
			<div class="search-area">
			<div class="logo">
				<a href="<?php echo __ROOT__?>" title="蘑菇街首页"></a>
			</div>
			
			<!--搜索框 区域-->
			<div class="search">
				<div class="search-box">
					<div class="selectbox">
                		<span class="selected">搜商品</span>
               		</div>
               		<!--form 表单 开始-->
               		<form action="H_S.html" method="get" id="sea-form">
               			<input type="text" value="<?php echo isset($_GET['words'])?$_GET['words']:'上衣'?>" class="ts-txt" name="words"/>
               			<input type="submit" value="搜  索" class="ts-btn"/>
               		</form>
               		<!--form 表单 结束-->
               	</div>
               	
               	<!--栏目列表  开始-->
               	<div class="cate-list">
               		<?php foreach ($cateData_cache as $v){?>
               		<a href="H_S.html?words=<?php echo $v['cname']?>"><?php echo $v['cname']?></a>
               		<?php }?>
               	</div>
               	<!--栏目列表 结束-->
				
			</div>
			<!--搜索框 结束-->
			
			<!--二维码 开始-->
	       		<!-- <div class="e-qrcode">
	                <img src="<?php echo __PUBLIC__?>/Home/images/ecode.png" alt="蘑菇街客户端下载" height="70" width="70">
	                <p>蘑菇街客户端</p>
	            </div> -->
       		<!--结束-->
		</div>
		</div>
		<!--搜索区域   结束-->
		
		<?php if( 'Userinfo'=='Index' && 'site'=='index' ){?>
                
		<!--菜单列表 开始-->
		<div class="menu-area">
			<div class="menu">
				<ul class="menu-list">
					<li class="home">全部商品</li>
									<?php
					$model = new \Common\Model\Category();
					$data = $model->where('pid=0 AND is_show=1')->limit(8)->get();
					foreach($data as $field):
					//列表页地址
					$field['url'] =__ROOT__ .  "/H_L_i_". $field['cid'] . ".html";
				?>
					
					<li><a href="<?php echo $field['url']?>"><?php echo $field['cname']?></a></li>
					
				<?php endforeach;?>
				</ul>
			</div>
		</div>
		<!--菜单列表 结束-->
		
               <?php }?>
		
		<!--公共头部样式 结束-->
		<script type="text/javascript">
			var siteUrl = "<?php echo U('Common/site')?>";
			var words = "<?php echo isset($oldSiteData['location'])?$oldSiteData['location']:''?>";
			var defaultUrl = "<?php echo U('Common/siteDefault')?>";
			var delSiteUrl = "<?php echo U('Common/delSite')?>";
		</script>
		
		<!--下滑线2px-->
		<div class="overstriking"></div>
		
		<!--公共头部样式 结束-->
		
		<!--主体内容 开始-->
		<div class="fm1200 clearfloat">
			<div class="meu-warp">
				<!--用户信息 左侧 开始-->
				<div class="mu-nav-wrap">
<div class="nav-info">
<!--头像-->
<div class="nav-info-avatar">
<a href="H_U.html"><div class="mu_nav_info_avatar_mk"></div></a>

<?php if($face){?>
                
<img src="<?php echo __ROOT__?>/<?php echo $face?>" height="100" width="100">
<?php }else{?>
<img src="<?php echo __PUBLIC__?>/Home/images/default_100x100.jpg" height="100" width="100">

               <?php }?>

</div>

<!--用户名-->
				<?php
					$userModel = new \Common\Model\User();
					$nickname = $userModel->where(array('uid'=>$_SESSION['uid']))->pluck('nickname');
				?>
			
<a href="H_U.html"><p class="mu_nav_info_uname"><?php echo $nickname?></p></a>

<a style="width:45px;" class="mu_nav_info_ulot">
<span id="vip_level" class="vip_level0" style="float:left;width: 45px; display: inline-block; height: 14px; margin-top: 3px;"></span>
</a>
</div>

<!--我的订单-->
<dl class="mu-nav">
<dt>我的订单</dt>
<dd <?php if($status==0 && 'site'=='myIndent' ){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_0.html" <?php if($status==0 && 'site'=='myIndent' ){?>
                class="red"
               <?php }?>>全部订单<?php if($status==0 && 'site'=='myIndent' ){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?></a>
</dd>
<dd <?php if($status==1){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_1.html" <?php if($status==1){?>
                class="red"
               <?php }?>>待付款 <?php if($status==1){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?></a>
</dd>
<dd <?php if($status==2){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_2.html" <?php if($status==2){?>
                class="red"
               <?php }?>>待发货<?php if($status==2){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?> </a>
</dd>
<dd <?php if($status==3){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_3.html" <?php if($status==3){?>
                class="red"
               <?php }?>>待收货<?php if($status==3){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?> </a>
</dd>
<dd <?php if($status==4){?>
                class="cur"
               <?php }?>>
<a href="H_U_mt_4.html" <?php if($status==4){?>
                class="red"
               <?php }?>>已完成<?php if($status==4){?>
                <span style="color: #ff5896;padding:0px 3px;font-weight: 700;"><?php echo $tatol?></span>
               <?php }?> </a>
</dd>

</dl>
<!--地址管理-->
<dl class="mu-nav">
<dt><a href="H_U_site_0.html">地址管理</a></dt>
</dl>

<dl class="mu-nav">
<dt><a href="javascript:;" class="hd-success-area">我的钱包</a></dt>
</dl>
<!--账号设置-->
<dl class="mu-nav">
<dt>帐号设置</dt>
<dd <?php if( 'Userinfo'=='Userinfo' && 'site'=='userinfo' ){?>
                class="cur"
               <?php }?>> <a href="H_U.html" <?php if( 'Userinfo'=='Userinfo' && 'site'=='userinfo' ){?>
                class="red"
               <?php }?>>基本信息</a> </dd>
<dd <?php if( 'Userinfo'=='Userinfo' && 'site'=='editFace' ){?>
                class="cur"
               <?php }?>> <a href="H_U_face.html" <?php if( 'Userinfo'=='Userinfo' && 'site'=='editFace' ){?>
                class="red"
               <?php }?>>修改头像</a> </dd>
</dl>
</div>

				<!--用户信息 左侧 结束-->
				
				<!--用户右侧个人信息  开始-->
				<div class="mu_location_wrap">
					<h2 class="lt-title">地址管理</h2>
					<div class="addr-edit clearfloat">
						<div class="add_new_addr">
							<span id="J_saddAddress">+添加地址</span>
						</div>
						
						<!--隐藏地址栏区域 开始-->
						<form action="" method="post" id="siteDataFrom">
									<div  id="addressbox_warp" <?php if($_GET['sid']){?>
                class="addressbox_warp"<?php }else{?>class="addressbox_warp hidden"
               <?php }?>>
										<div class="addressbox">
											<div class="address-pop-area">
												<table class="hd-table hd-table-form hd-form">
													<tbody>
														<tr>
															<th width="100">省:&nbsp<span class="red">*</span></th>
															<td>
																<select name="province" id="province">
    				                								<option value="">请选择</option>
    															</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            					<select name="city" id="city">
                                                      				<option value="">请选择</option>
                                              					</select>
															</td>
														</tr>
														<tr>
															<th>邮政编码:&nbsp<span class="red">*</span></th>
															<td>
																<input type="text" class="input r3" name="postalcode" value="<?php echo isset($oldSiteData['postalcode'])?$oldSiteData['postalcode']:''?>">
															</td>
														</tr>
														<tr>
															<th>详细地址:&nbsp<span class="red">*</span></th>
															<td>
																<textarea name="street"  rows="10" id="text-area" placeholder="请填写详细的街道地址，最少5个字，最多不能超过100个字^_^"><?php echo isset($oldSiteData['street'])?$oldSiteData['street']:''?></textarea>
															</td>
														</tr>
														<tr>
															<th>收货人姓名:&nbsp<span class="red">*</span></th>
															<td>
																<input type="text" class="input r3" name="consignee" value="<?php echo isset($oldSiteData['consignee'])?$oldSiteData['consignee']:''?>">
															</td>
														</tr>
														<tr>
															<th>手机:&nbsp<span class="red">*</span></th>
															<td>
																<input type="text" class="input r3" name="phone" value="<?php echo isset($oldSiteData['phone'])?$oldSiteData['phone']:''?>">
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<!--基本资料 结束-->
											<div class="last-area" style="padding-left:123px ;">
												<input type="hidden" name="sid" value="<?php echo isset($oldSiteData['sid'])?$oldSiteData['sid']:''?>"/>
												<button class="hd-btn hd-btn-danger" type="submit" live="1" id="siteLive">确认</button>
												<button class="hd-btn hd-btn-default" id="hd-del" type="button">取消</button>
											</div>
										</div>
									</div>
									
									</form>
									
						<!--隐藏地址栏区域 结束-->
					</div>
					<?php if($siteData){?>
                
					<?php foreach ($siteData as $v){?>
					<!--显示地址 开始-->
					<div style="margin-bottom:5px;"<?php if($v['status']){?>
                class="addr_section addr-section-cur clearfloat"<?php }else{?>class="addr_section clearfloat"
               <?php }?>>
						<i class="section-cur-g"></i>
						<ul class="clearfloat">
		                    <li class="name"><?php echo $v['consignee']?></li>
		                    <li class="addr"><?php echo $v['location']?>&nbsp<?php echo $v['street']?></li>
		                    <li class="zcode"><?php echo $v['postalcode']?></li>
		                    <li class="mobile"><?php echo $v['phone']?></li>
		                    <li class="oper">
		                        <a href="javascript:;" sid="<?php echo $v['sid']?>" status="<?php echo $v['status']?>" <?php if($v['status']){?>
                class="J_default J-default-a"<?php }else{?>class="J_default"
               <?php }?>><?php if($v['status']){?>
                已为默认<?php }else{?>设为默认
               <?php }?></a>
		                        <a href="H_U_site_<?php echo $v['sid']?>.html" class="edit cur">编辑</a>
		                        <a href="javascript:;" class="del nobd del-cur" sid="<?php echo $v['sid']?>">删除</a>
		                    </li>
		                    <li class="enaddr"></li>
           				</ul>
           			</div>
           			<?php }?>
           			<!--显示地址 结束-->
           			
               <?php }?>
				</div>
				<!--用户右侧个人信息  结束-->
			</div>
		</div>
		<!--主体内容 结束-->
		
		
		<!--底部公共部分   开始-->
		<!--载入尾部-->
		<!--底部公共部分   开始-->
		<!--底部 开始-->
		<div class="floor-area clearfloat" style="margin-bottom: 10px;">
			<div class="floor">
				<div class="foot-info">
		            <a class="info-logo" href="#"></a>
		            <div class="info-text">
		                <p>站点名称：<a href="" target="_blank"><?php echo C('webset.webname')?></a></p>
		               	<p class="mgjhostname" title="guomai31072"><?php echo C('webset.webdes')?></p>
		            </div>
    			</div>
    			
				<div class="foot_link">
		            <dl class="link_company">
		                <dt>友情链接</dt>
		                <?php foreach ($linkData as $v){?>
		                <dd><a href="<?php echo $v['url']?>" target="_blank"><?php echo $v['lname']?></a></dd>
		                <?php }?>
		            </dl>
		          
        		</div>
			</div>
		<!-- 	<?php if( 'Userinfo'=='Index' && 'site'=='index' ){?>
                
			<div class="w-links clearfloat">
        			<ul>
	            		<li>友情链接: </li>
	                    <li><a target="_blank" href="#">淘粉吧</a></li>
	                    <li><a target="_blank" href="#">蘑菇街团购网</a></li>
	                    <li><a target="_blank" href="#">蘑菇街女装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街男装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街鞋子</a></li>
	                    <li><a target="_blank" href="#">蘑菇街包包</a></li>
	                    <li><a target="_blank" href="#">蘑菇街家居</a></li>
	                    <li><a target="_blank" href="#">家具网</a></li>
	                    <li><a target="_blank" href="#">时尚品牌网</a></li>
	                    <li><a target="_blank" href="#">装修</a></li>
	                    <li><a target="_blank" href="#">蘑菇街母婴</a></li>
                	</ul>
    		</div>
    		
               <?php }?> -->
    		
		</div>
		<!--底部 结束-->
		<!--底部公共部分   结束-->
		
		<?php if( 'Userinfo'=='Index' && 'site'=='index' ){?>
                
		<!--首页头部 弹出搜索框 开始-->
		<div class="sticky-search-container">
			<div class="fix-warp">
				<a href="#" class="logo" title="蘑菇街|我的买手街">蘑菇街|我的买手街</a>
				<div class="nav_search_form">
					<div class="search_inner_box">
						<div class="selectbox">
							<span class="selected">搜商品</span>
						</div>
						<form action="H_S.html" method="get" id="top_nav_form">
                            <input class="fx-txt" value="<?php echo isset($_GET['words'])?$_GET['words']:'裙子'?>"  type="text" name="words">
							<input value="搜  索" class="fx_btn"  type="submit">
            			</form>
					</div>
				</div>
			</div>
		</div>
		<!--首页头部 弹出搜索框 结束-->
		
               <?php }?>
		
		<!--用户信息除外-->
		<?php if( 'Userinfo'!='Userinfo' && 'Userinfo'!='Cart' && 'site'!='userinfo' ){?>
                
		<!--右侧购物车 回到顶部 相对定位 开始-->
		<div class="mgj_rightbar">
			<!--用户头像-->
			<div class="sidebar-item mgj-my-avatar">
				<a href="javascript:;" id="mg-userFace" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>">
					<?php if(isset($_SESSION['uid'])){?>
                
					<div class="img">
						<img src="<?php echo __ROOT__?>/<?php echo $face?>"  height="20" width="20">
					</div>
					<?php }else{?>
					<div class="img">
						<img src="<?php echo __PUBLIC__?>/Home/images/02.jpg_48x48.jpg" height="20" width="20">
					</div>
					
               <?php }?>
				</a>
			</div>
			<!--购物车-->
			<div class="sidebar-item mgj-my-cart" id="mgj-my-cart">
        		<a data-ptp-idx="2" target="_blank" href="<?php echo __ROOT__?>/H_C_i.html">
            		<i class="s-icon"></i>
            		<div class="s-txt">购物车</div>
            		<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
            		<div class="num floorNum"><?php echo $_SESSION['tatol']?></div>
            		
               <?php }?>
        		</a>
        		
        		<!--隐藏购物车div 开始-->
        		<div class="cart-hide-area hidden" id="cart-hide-area">
        			<p class="p1">
        				<span class="mac-success-txt module-cart-icons">已将商品添加到购物车</span>
        			</p>
        			<p><a href="<?php echo __ROOT__?>/H_C_i.html" class="mac-go-cart module-cart-icons">去购物车结算</a></p>
        			<a href="javascript:;" class="J_Close fix-close-btn">关闭</a>
        			<span class="sanxiao"></span>
        		</div>
        		
        		<!--隐藏购物车div 结束-->
    		</div>
    		<!--回到顶部-->
    		<div class="sideBottom">
       			<div class="sidebar-item mgj-back2top">
		            <a rel="nofollow" href="javascript:;">
		                <i class="s-icon"></i>
		            </a>
		        </div>
    		</div>
		</div>
		<!--右侧购物车 回到顶部 相对定位 结束-->
		
               <?php }?>
		
	</body>
<!-- 载入JS区域 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/jquery-1.8.3.min.js" ></script>

<!-- 载入HDjs -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>
<!-- 载入城市联动 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/city.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/formValidate.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/index.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/common.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/slide.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/goods.js"></script>




<!-- 载入Uploadify上传插件 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/method.js"></script>

</html>


