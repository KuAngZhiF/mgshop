<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>蘑菇街-支付页</title>
	</head>
	<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/common.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/floor.css"/>
		
		<!-- 载入HDjs样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/cart.css"/>
		<script type="text/javascript">
				var Root = "<?php echo __ROOT__?>";
				var payAjaxUrl = "<?php echo U('Common/payAjax')?>";
				
		</script>
	<body>
		<!--头部 开始-->
		<div class="header-area hea-cart">
			<div class="header">
				<a href="<?php echo __ROOT__?>" class="home">蘑菇街首页</a>
				<ul class="header-top">
					<!--用户名登录后 的div-->
					<?php if(isset($_SESSION['uid'])?$_SESSION['uid']:0){?>
                
					<li class="t1 has_icon user_meta" id="user_meta">
	                    <a href="javascript:;">很纯很暧昧777</a>
	                    <a href="" target="_blank"><span class="user-level user-level0">&nbsp;</span></a>
	                    <i class="icon_delta"></i>
	                    <ol class="ext_mode" id="menu_personal" style="display: none;">
	                        <li class="s2"><a target="_blank" href="<?php echo U('Userinfo/userinfo')?>">个人设置</a></li>
	                        <li class="s2"><a target="_blank" href="<?php echo U('Userinfo/userinfo')?>">账号绑定</a></li>
	                        <li class="s2"><a rel="nofollow" href="<?php echo U('Login/LoginOut')?>">退出</a></li>
	                    </ol>
                	</li>
                	<!--用户名登录后 的div 结束-->
                	<?php }else{?>
                	<!--没登录状态-->
					<li class="t1"><a href="H_reg.html">注册</a></li>
					<li class="t1"><a href="H_log.html">登录</a></li>
					<!--没登录状态 结束-->
					
               <?php }?>
					<li class="t1 myorder t1-line" id="J-order" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>"><a href="javascript:;">我的订单</a></li>
					<li class="t1 myorder t1-cate" id="t1-cate">
						<a href="H_C_i.html">购物车
							<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
							<span class="floorNum" style="color: #f36;padding: 0px;font-weight: 400;"><?php echo $_SESSION['tatol']?></span>
							<span>件</span>
							
               <?php }?>
						</a>
						<!--购物车隐藏盒子 开始-->
						
						<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
						<!--有商品的时候-->
						<div class="cate-hide cate-info" style="display: none;">
							<ul>
								<?php foreach ($_SESSION['goods'] as $v){?>
	                            <li>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="imgbox">
										<img src="<?php echo __ROOT__?>/<?php echo $v['pic']?>"  width="45" height="45">
									</a>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="title"><?php echo $v['gname']?></a>
									<span class="info">
											<?php foreach ($v['options'] as $gtname=>$vo){?>
									    	<span><?php echo $gtname?>：<?php echo $vo?></span>
									    	<?php }?>
									</span>
									<span class="price">￥<?php echo $v['shopprice']?></span>
									<a href="javascript:;" class="del delete" mgprice="<?php echo $v['price']?>" glid="<?php echo $v['glid']?>">删除</a>
								</li>
								<?php }?>
							</ul>
							<div class="subbox">
                    			<div class="fr">
                				<a href="H_C_i.html" target="_blank" class="goel">查看购物车</a>
            					</div>
        					</div>
						</div>
						<!--购物车隐藏盒子 结束-->
						<?php }else{?>
						<!--没有商品的时候-->
						<div class="cate-hide empty_cart" style="display: none;">
							购物车里没有商品！
						</div>
						<!--没有商品的时候 结束-->
						
               <?php }?>
					</li>
				</ul>
				
			</div>
		</div>
		<!--头部 结束-->
		
		<!--购物车 开始-->
		<div class="g-warp">
			<!--购物车下单 头部 开始-->
			<div class="g-header">
				<a href="" class="g-lf g-lf-pay"></a>
				<div class="g-rg">
					<div class="md_process_wrap md_process_step2_5">
        				<div class="md_process_pay"></div>
                    	<i class="md_process_i md_process_i1">
                			1<span class="md_process_tip">购物车</span>
            			</i>
                    	<i class="md_process_i md_process_i2">
			                2<span class="md_process_tip">确认订单</span>
			            </i>
                    	<i class="md_process_i md_process_i3">
                			3<span class="md_process_tip">支付</span>
            			</i>
                    	<i class="md_process_i md_process_i4">
                            <img src="<?php echo __PUBLIC__?>/Home/images/right.png" alt="" height="23" width="23">
                            <span class="md_process_tip">完成</span>
            			</i>
            		</div>
				</div>
			</div>
			<!--购物车下单 头部 结束-->
			
			<!--购物横线-->
			<div class="h-line clearfloat"></div>
			
			<!--支付信息 中间的页面-->
			<div class="pay-area">
				<div class="md-order-head clearfloat">
					<h2 class="tit">
						<span>订单提交成功</span>
					</h2>
					<span class="mon">
        				<span>应付金额：</span>
        				<span class="mon-num">
            				<span>¥</span> <span><?php echo $totalPrice?></span>
        				</span>
    				</span>
				</div>
				
				<div class="md-discount clearfloat">
					<div class="banklist-btm">
						<div class="banklist-item">
							<i class="product-icon icon-overage"></i>
							<span class="earnname">货到付款</span>
							<span class="earntip"><input type="radio" name="paymethod"  value="1"/>&nbsp是&nbsp&nbsp<input type="radio" name="paymethod" value="0"  checked="checked"/>&nbsp否</span>
						</div>
					</div>
					
					<div class="banklist-btm">
						<div class="banklist-item">
							<i class="product-icon icon-overages"></i>
							<span class="earnname">余额</span>
							<span class="earntip" id="J_Num">可用余额<span style="color:#F36;font-weight: 700;padding: 0px 2px;">￥</span><span class="num" style="color:#F36;padding: 0px 0px;"><?php echo $pay?></span>元</span>
						</div>
					</div>
					
				</div>
				
				<div class="md-order-foot2 clearfloat">
					<input type="hidden" name="oid" value="<?php echo $_GET['oid']?>" id="J_Oid">
					<a href="javascript:;" class="J_payOrder">确认并付款</a>
					<span class="mon2">
    					<span>实付金额：</span>
    					<span id="mon-num2">
      						<span style="color: #F36; font-size: 14px;font-weight: 700;">¥</span>
  							<span style="color: #F36;font-size: 14px;font-weight: 700;" id="J_Goods_num"><?php echo $totalPrice?></span>
						</span>
  					</span>
				</div>
				
			</div>
			<!--支付信息 中间的页面 结束-->
			
		</div>	
		
		<!--底部公共部分   开始-->
		<!--载入尾部-->
		<!--底部公共部分   开始-->
		<!--底部 开始-->
		<div class="floor-area clearfloat" style="margin-bottom: 10px;">
			<div class="floor">
				<div class="foot-info">
		            <a class="info-logo" href="#"></a>
		            <div class="info-text">
		                <p>站点名称：<a href="" target="_blank"><?php echo C('webset.webname')?></a></p>
		               	<p class="mgjhostname" title="guomai31072"><?php echo C('webset.webdes')?></p>
		            </div>
    			</div>
    			
				<div class="foot_link">
		            <dl class="link_company">
		                <dt>友情链接</dt>
		                <?php foreach ($linkData as $v){?>
		                <dd><a href="<?php echo $v['url']?>" target="_blank"><?php echo $v['lname']?></a></dd>
		                <?php }?>
		            </dl>
		          
        		</div>
			</div>
		<!-- 	<?php if( 'Cart'=='Index' && 'alipay'=='index' ){?>
                
			<div class="w-links clearfloat">
        			<ul>
	            		<li>友情链接: </li>
	                    <li><a target="_blank" href="#">淘粉吧</a></li>
	                    <li><a target="_blank" href="#">蘑菇街团购网</a></li>
	                    <li><a target="_blank" href="#">蘑菇街女装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街男装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街鞋子</a></li>
	                    <li><a target="_blank" href="#">蘑菇街包包</a></li>
	                    <li><a target="_blank" href="#">蘑菇街家居</a></li>
	                    <li><a target="_blank" href="#">家具网</a></li>
	                    <li><a target="_blank" href="#">时尚品牌网</a></li>
	                    <li><a target="_blank" href="#">装修</a></li>
	                    <li><a target="_blank" href="#">蘑菇街母婴</a></li>
                	</ul>
    		</div>
    		
               <?php }?> -->
    		
		</div>
		<!--底部 结束-->
		<!--底部公共部分   结束-->
		
		<?php if( 'Cart'=='Index' && 'alipay'=='index' ){?>
                
		<!--首页头部 弹出搜索框 开始-->
		<div class="sticky-search-container">
			<div class="fix-warp">
				<a href="#" class="logo" title="蘑菇街|我的买手街">蘑菇街|我的买手街</a>
				<div class="nav_search_form">
					<div class="search_inner_box">
						<div class="selectbox">
							<span class="selected">搜商品</span>
						</div>
						<form action="H_S.html" method="get" id="top_nav_form">
                            <input class="fx-txt" value="<?php echo isset($_GET['words'])?$_GET['words']:'裙子'?>"  type="text" name="words">
							<input value="搜  索" class="fx_btn"  type="submit">
            			</form>
					</div>
				</div>
			</div>
		</div>
		<!--首页头部 弹出搜索框 结束-->
		
               <?php }?>
		
		<!--用户信息除外-->
		<?php if( 'Cart'!='Userinfo' && 'Cart'!='Cart' && 'alipay'!='userinfo' ){?>
                
		<!--右侧购物车 回到顶部 相对定位 开始-->
		<div class="mgj_rightbar">
			<!--用户头像-->
			<div class="sidebar-item mgj-my-avatar">
				<a href="javascript:;" id="mg-userFace" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>">
					<?php if(isset($_SESSION['uid'])){?>
                
					<div class="img">
						<img src="<?php echo __ROOT__?>/<?php echo $face?>"  height="20" width="20">
					</div>
					<?php }else{?>
					<div class="img">
						<img src="<?php echo __PUBLIC__?>/Home/images/02.jpg_48x48.jpg" height="20" width="20">
					</div>
					
               <?php }?>
				</a>
			</div>
			<!--购物车-->
			<div class="sidebar-item mgj-my-cart" id="mgj-my-cart">
        		<a data-ptp-idx="2" target="_blank" href="<?php echo __ROOT__?>/H_C_i.html">
            		<i class="s-icon"></i>
            		<div class="s-txt">购物车</div>
            		<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
            		<div class="num floorNum"><?php echo $_SESSION['tatol']?></div>
            		
               <?php }?>
        		</a>
        		
        		<!--隐藏购物车div 开始-->
        		<div class="cart-hide-area hidden" id="cart-hide-area">
        			<p class="p1">
        				<span class="mac-success-txt module-cart-icons">已将商品添加到购物车</span>
        			</p>
        			<p><a href="<?php echo __ROOT__?>/H_C_i.html" class="mac-go-cart module-cart-icons">去购物车结算</a></p>
        			<a href="javascript:;" class="J_Close fix-close-btn">关闭</a>
        			<span class="sanxiao"></span>
        		</div>
        		
        		<!--隐藏购物车div 结束-->
    		</div>
    		<!--回到顶部-->
    		<div class="sideBottom">
       			<div class="sidebar-item mgj-back2top">
		            <a rel="nofollow" href="javascript:;">
		                <i class="s-icon"></i>
		            </a>
		        </div>
    		</div>
		</div>
		<!--右侧购物车 回到顶部 相对定位 结束-->
		
               <?php }?>
		
	</body>
<!-- 载入JS区域 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/jquery-1.8.3.min.js" ></script>

<!-- 载入HDjs -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>
<!-- 载入城市联动 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/city.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/formValidate.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/index.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/common.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/slide.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/goods.js"></script>




<!-- 载入Uploadify上传插件 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/method.js"></script>

</html>


		