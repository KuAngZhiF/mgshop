<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>蘑菇街-详情页</title>
		<!--载入头部-->
				<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/common.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/floor.css"/>
		
		<!-- 载入HDjs样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css"/>
		<!-- 首页样式只有首页有 -->
		<?php if( 'Details'=='Index' && 'index'=='index' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/index.css"/>
		
               <?php }?>
		
		<?php if( 'Details'=='Userinfo' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/userinfo.css"/>
		
               <?php }?>
		
		<?php if( 'Details'=='Lists' ){?>
                
		<!-- 列表页样式  -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/lists.css"/>
		
               <?php }?>
		
		<?php if( 'Details'=='Details' ){?>
                
		<!-- 详情页 样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/details.css"/>
		
               <?php }?>
		
		<!-- 购物车样式 -->
		<?php if( 'Details'=='Cart' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/cart.css"/>
		
               <?php }?>
		
		<?php if( 'Details'=='Userinfo' && 'index'=='orderdetail' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/indent.css"/>
		
               <?php }?>
		
		<!-- 搜索页样式 -->
		<?php if( 'Details'=='Search' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/seek.css" />
		
               <?php }?>
		
		<!-- 载入上传Uploadify样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">
		
		<script type="text/javascript">
				var Public = "<?php echo __PUBLIC__?>";
				var Root = "<?php echo __ROOT__?>";
				var sname = "<?php echo session_name();?>";
				var sid = "<?php echo session_id();?>";
				var userFaceUrl = "<?php echo U('Userinfo/face')?>";
				var payValueUrl = "<?php echo U('Common/payValue')?>";
		</script>
		
		
	</head>
	<body>
		<!-- HD模态框  开始 -->
		<div id="hd-modal-bg" style="opacity:0.7;filter:alpha(opacity=7);display:none;">背景遮罩</div>
		<div id="hd-modal" style="width:300px;height:180px;margin-left:-180px; display:none;">        
				<div class="hd-modal-wrap" style="height:180px">            
						 <div class="hd-modal-header">                
							<span class="hd-modal-title">余额充值</span>               
						 	<span class="hd-modal-close hd-clone-modal"></span>            
						  </div>            
				 		  <div class="hd-modal-message">
				 		  		<textarea name="pay" rows="4" id="text-area"  style="width:280px;" class="text-pay-value"></textarea>
				 		  </div>           
						  <div class="hd-modal-btn">                
							  <a class="hd-btn hd-btn-success hd-success-modal" href="javascript:;">确认充值</a>&nbsp;                
							  <a class="hd-btn hd-clone-modal" href="javascript:;">关闭</a>           
						   </div>        
				</div>    
		</div>
		<!-- HD模态框  结束 -->
		
		<!--公共头部样式 开始-->
		<!--头部 开始-->
		<div class="header-area">
			<div class="header">
				<a href="<?php echo __ROOT__?>" class="home">蘑菇街首页</a>
				<ul class="header-top">
					<!--用户名登录后 的div-->
					<?php if(isset($_SESSION['uid'])?$_SESSION['uid']:0){?>
                
					<li class="t1 has_icon user_meta" id="user_meta">
	                    <a href="javascript:;">很纯很暧昧777</a>
	                    <a href="" target="_blank"><span class="user-level user-level0">&nbsp;</span></a>
	                    <i class="icon_delta"></i>
	                    <ol class="ext_mode" id="menu_personal" style="display: none;">
	                        <li class="s2"><a target="_blank" href="H_U.html">个人设置</a></li>
	                        <li class="s2"><a target="_blank" href="H_U.html">账号绑定</a></li>
	                        <li class="s2"><a rel="nofollow" href="H_logout.html">退出</a></li>
	                    </ol>
                	</li>
                	<!--用户名登录后 的div 结束-->
                	<?php }else{?>
                	<!--没登录状态-->
					<li class="t1"><a href="H_reg.html">注册</a></li>
					<li class="t1"><a href="H_log.html">登录</a></li>
					<!--没登录状态 结束-->
					
               <?php }?>
					<li class="t1 myorder t1-line" id="J-order" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>"><a href="javascript:;">我的订单</a></li>
					<li class="t1 myorder t1-cate" id="t1-cate">
						<a href="H_C_i.html">购物车
							<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
							<span class="floorNum" style="color: #f36;padding: 0px;font-weight: 400;"><?php echo $_SESSION['tatol']?></span>
							<span>件</span>
							
               <?php }?>
						</a>
						<!--购物车隐藏盒子 开始-->
						
						<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
						<!--有商品的时候-->
						<div class="cate-hide cate-info" style="display: none;">
							<ul>
								<?php foreach ($_SESSION['goods'] as $v){?>
	                            <li>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="imgbox">
										<img src="<?php echo __ROOT__?>/<?php echo $v['pic']?>"  width="45" height="45">
									</a>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="title"><?php echo $v['gname']?></a>
									<span class="info">
											<?php foreach ($v['options'] as $gtname=>$vo){?>
									    	<span><?php echo $gtname?>：<?php echo $vo?></span>
									    	<?php }?>
									</span>
									<span class="price">￥<?php echo $v['shopprice']?></span>
									<a href="javascript:;" class="del delete" mgprice="<?php echo $v['price']?>" glid="<?php echo $v['glid']?>">删除</a>
								</li>
								<?php }?>
								
							</ul>
							<div class="subbox">
                    			<div class="fr">
                				<a href="H_C_i.html" target="_blank" class="goel">查看购物车</a>
            					</div>
        					</div>
						</div>
						<!--购物车隐藏盒子 结束-->
						<?php }else{?>
						<!--没有商品的时候-->
						<div class="cate-hide empty_cart" style="display: none;">
							购物车里没有商品！
						</div>
						<!--没有商品的时候 结束-->
						
               <?php }?>
					</li>
				</ul>
				
			</div>
		</div>
		<!--头部 结束-->
		
		<!--搜索区域   开始-->
		<div class="search-warp">
			<div class="search-area">
			<div class="logo">
				<a href="<?php echo __ROOT__?>" title="蘑菇街首页"></a>
			</div>
			
			<!--搜索框 区域-->
			<div class="search">
				<div class="search-box">
					<div class="selectbox">
                		<span class="selected">搜商品</span>
               		</div>
               		<!--form 表单 开始-->
               		<form action="H_S.html" method="get" id="sea-form">
               			<input type="text" value="<?php echo isset($_GET['words'])?$_GET['words']:'上衣'?>" class="ts-txt" name="words"/>
               			<input type="submit" value="搜  索" class="ts-btn"/>
               		</form>
               		<!--form 表单 结束-->
               	</div>
               	
               	<!--栏目列表  开始-->
               	<div class="cate-list">
               		<?php foreach ($cateData_cache as $v){?>
               		<a href="H_S.html?words=<?php echo $v['cname']?>"><?php echo $v['cname']?></a>
               		<?php }?>
               	</div>
               	<!--栏目列表 结束-->
				
			</div>
			<!--搜索框 结束-->
			
			<!--二维码 开始-->
	       		<!-- <div class="e-qrcode">
	                <img src="<?php echo __PUBLIC__?>/Home/images/ecode.png" alt="蘑菇街客户端下载" height="70" width="70">
	                <p>蘑菇街客户端</p>
	            </div> -->
       		<!--结束-->
		</div>
		</div>
		<!--搜索区域   结束-->
		
		<?php if( 'Details'=='Index' && 'index'=='index' ){?>
                
		<!--菜单列表 开始-->
		<div class="menu-area">
			<div class="menu">
				<ul class="menu-list">
					<li class="home">全部商品</li>
									<?php
					$model = new \Common\Model\Category();
					$data = $model->where('pid=0 AND is_show=1')->limit(8)->get();
					foreach($data as $field):
					//列表页地址
					$field['url'] =__ROOT__ .  "/H_L_i_". $field['cid'] . ".html";
				?>
					
					<li><a href="<?php echo $field['url']?>"><?php echo $field['cname']?></a></li>
					
				<?php endforeach;?>
				</ul>
			</div>
		</div>
		<!--菜单列表 结束-->
		
               <?php }?>
		
		<!--公共头部样式 结束-->
		<script type="text/javascript">
				var DetUrl = "<?php echo U('Details/detList')?>";
				var addUrl = "<?php echo U('Details/addDet')?>";
				var minusUrl = "<?php echo U('Details/minusDet')?>";
				var joinCart = "<?php echo U('Details/joinCart')?>";
		</script>
		<div class="overstriking"></div>
		<!--公共头部样式 结束-->
		
		<!--商品主要信息  开始-->
		<div class="detail-goods">
			<div class="dgd-left">
				<!--商品图片 开始-->
				<div class="dgd-img">
					<!--大图-->
					<div class="big-img" id="mid-imgs">
						<button class="middle">
							<img src="<?php echo __ROOT__?>/<?php echo $firstImgs?>"/>
                        </button>
					</div>
					<!--小图切换-->
					<div class="min-img">
						<div class="box">
							<div class="list" id="goodsMin-list">
								<ul class="m-carousel">
									<?php foreach ($imgsData as $k=>$v){?>
									<li <?php if($k==0){?>
                class="c"
               <?php }?>>
										<img src="<?php echo __ROOT__?>/<?php echo $v['mini']?>"/>
										<i <?php if($k==0){?>
                style="display:block"
               <?php }?>></i>
									</li>
									<?php }?>
								</ul>
							</div>
							<a href="javascript:;" class="min-left"></a>
							<a href="javascript:;" class="min-right"></a>
						</div>
					</div>
				</div>
				<!--商品图片 结束-->
				
				<!--商品信息 开始 左侧-->
				<div class="dgd-infos">
					<h1 class="goods-title">
						<span><?php echo $imgData['gname']?></span>
					</h1>
					<!--价格区域 开始-->
					<div class="gds-info-lt">
	    				<dl class="price-info" style="height:22px;">
							<dt class="price-origin">原价：</dt>
							<dd class="price-w">
								<span>¥</span>
								<span id="J_Price"><?php echo $imgData['marketprice']?></span>
							</dd>
						</dl>
        				
        				<dl style="padding-bottom:0;height:40px;">
							<dt style="line-height:45px;">现价：</dt>
							<dd>
					            <span class="normal-price-now">¥ </span>
								<span id="J_NowPrice" class="normal-price-now" itemprop="price"><?php echo $imgData['shopprice']?></span>
								<span class="fr normal-rate-sales" style="padding-top:15px;">
									<span class="rate-num">评价：<span class="num"><?php echo rand(100, 500);?></span></span>
	                				<span>累计销量：<span class="num J_SaleNum"><?php echo rand(500, 1000);?></span></span>
	                				</span>
							</dd>
						</dl>
						
					</div>
					<!--价格区域 结束-->
					<form action="C_i.html" method="post" id="cartForm">
					<!--商品内容选择 开始-->
					<div class="gds-content">
						<?php foreach ($listData as $gtname=>$v){?>
						<div class="gds-cor">
							<div class="gds-lf"><?php echo $gtname?>：</div>
							<div class="gds-rg" id="gds-rg">
								<input type="hidden" name="gid" value="<?php echo $_GET['gid']?>">
								<input type="hidden" name="gtid" value=""  id="gtid">
								<?php foreach ($v as $vo){?>
								<a href="javascript:;" class="img" gtid="<?php echo $vo['gt_id']?>" gid="<?php echo $vo['goods_gid']?>">
									<?php echo $vo['gt_name']?>
									<b></b>
								</a>
								<?php }?>
							</div>
						</div>
						<?php }?>
						
						<div class="gds-cor">
							<div class="gds-lf">数量：</div>
							<div class="gds-rg">
								<div class="goods-num">
									<span class="num-reduce num-disable" id="num-reduce"></span>
									<input class="num-input" value="1" type="text" id="num-input" disabled="disabled">
									<span class="num-add" id="num-add"></span>
								</div>
								<div class="goods-stock">库存<span id="inven-num">0</span>件</div>
								<div class="goods-stock-tip" style="display:none;" id="goods-stock-tip"></div>
							</div>
						</div>
					</div>
					<!--商品内容选择 结束-->
					
					<!--立即购买 加入购物车-->
					<div class="goods-buy clearfloat">
                        <a href="javascript:;" id="J_BuyNow" class="fl mr10 buy-btn buy-now">立刻购买</a>
                		<a href="javascript:;" id="J_BuyCart" class="fl mr10 buy-cart  buy-btn">加入购物车</a>
                    </div>
					<!--立即购买 加入购物车 结束-->
					</form>
					<!--点赞爱心 开始-->
					<div class="goods-social">
						<div class="fav item">
							<b></b>
							<span class="fav-num"><?php echo rand(300, 1000);?></span>
						</div>
					</div>
					<!--点赞爱心 结束-->
					
					<!--支付方式 开始-->
					<div class="pay-methods clearfloat">
						<div class="pa1">支付方式：</div>
						<div class="pa2"></div>
					</div>
					<!--支付方式 结束-->
				</div>
				<!--商品信息 结束 左侧-->
			</div>
			
			<!--商品右侧 图片展示 开始-->
			<div class="goods-right">
				<p class="title">
					<s></s><span>热卖推荐</span>
				</p>
				<div class="list">
				    <div class="box">
				        <ul>
				        					<?php
					$model = new \Common\Model\Goods();
					$data = $model->orderBy('hot','DESC')->limit(3)->field('gid,pic,shopprice')->get();
					foreach($data as $field):
					//详情页地址
					$field['url'] =__ROOT__ .  "/H_D_i_". $field['gid'] . ".html";
				?>
					
				            <li>
				                <a href="<?php echo $field['url']?>" target="_blank">
				                    <img src="<?php echo __ROOT__?>/<?php echo $field['pic']?>" width="120">
				                </a>
				                <span>￥<?php echo $field['shopprice']?></span>
				            </li>
				           
				<?php endforeach;?>
				        </ul>
				    </div>
				</div>
			</div>
			<!--商品右侧 图片展示 结束-->
		</div>
		<!--商品主要信息  结束 -->
		
		<!--商品详情展示 区域  开始-->
		<div class="warp-g clearfloat">
			<!--侧边栏 开始-->
			<div class="col-sidebar">
				<!--看了又看目录  开始-->
				<div class="module-repeat">
					<div class="ui-box repeat-info">
						<h3 class="ui-hd">看了又看</h3>
						<div class="ui-bd">
							<!--侧栏循环区域-->
							<ul class="repeat-list">
												<?php
					$model = new \Common\Model\Goods();
					$data = $model->orderBy('click','DESC')->limit(8)->field('gid,gname,pic,shopprice','click')->get();
					foreach($data as $field):
					//详情页地址
					$field['url'] =__ROOT__ .  "/H_D_i_". $field['gid'] . ".html";
				?>
					
								<li>
									<a href="<?php echo $field['url']?>" class="pic">
										<img src="<?php echo __ROOT__?>/<?php echo $field['pic']?>" class="img-lazyload"/>
									</a>
									<a class="title" href="<?php echo $field['url']?>" target="_blank">
										<?php echo msubstr($field['gname'],0,14)?>
									</a>
									<div class="info"> 
										<div class="price"> 
											<em class="price-u">¥</em> 
											<span class="price-n"><?php echo $field['shopprice']?></span> 
										</div> 
										<div class="fav"> 
											<em class="fav-i"></em> 
											<span class="fav-n"><?php echo $field['click']?></span> 
										</div> 
									</div>
								</li>
								
				<?php endforeach;?>
							</ul>
							<!--侧栏循环区域 结束-->
						</div>
					</div>
				</div>
				<!--看了又看目录  结束-->
			</div>
			<!--侧边栏 结束-->
			
			<!--商品详情 累计评价 中间区域 开始-->
			<div class="module-tabpanel" id="module-tabpanel">
				<!--选项卡区域 开始-->
				<div class="tabbar-box">
					<ul class="tabbar-list" id="tabbar-list">
						<li class="tab-graphic selected">
							<a href="javascript:;">商品详情</a>
						</li>
						<!-- <li>
							<a href="javascript:;">累计评价<em>124</em></a>
						</li> -->
					</ul>
				</div>
				<!--选项卡区域 结束-->
				
				<!--选项页  开始-->
				
				<!--商品详情  开始-->
				<div class="panel-box">
					<!--图文详情 开始-->
					<div class="module-panel module-graphic">
						<!--商品描述 开始-->
						<div id="J_Graphic_item_desc">
							<div class="panel-title"> 
								<h1>商品描述</h1> 
							</div>
							<div class="graphic-block"> 
								<!-- 描述 -->  
								<div class="graphic-text"><?php echo $imgData['intro']?></div>   
							</div>
						</div>
						<!--商品描述 结束-->
						
						<!-- 产品参数  开始-->
						<div id="J_Graphic_product_info">
							<div class="panel-title"> <h1>产品参数</h1> </div>
							<div class="graphic-block">  
								<!-- 描述 --> 
								<!-- 表格 --> 
								<table class="parameter-table" id="J_ParameterTable"> 
									<tbody> 
										<?php foreach ($attrData as $v){?>  
										<tr>   
											<td style="width:15%;"><?php echo $v['gtname']?></td>       
											<td><?php echo $v['gt_name']?></td> 
										</tr>   
									   <?php }?>
									</tbody> 
								</table>   
							</div>
						</div>
						<!-- 产品参数  结束-->
						
						<!--模块列表图片区域  开始-->
						<div id="J_Graphic_model_img">
							<div class="panel-title"> <h1>穿着效果</h1> </div>
							
							<div class="graphic-block">
								<!-- 图片列表 -->
								<?php foreach ($imgsData as $v){?>
								<div class="graphic-pic"> 
									<div class="pic-box" style="padding-top:525px;padding-bottom:525px;"> 
										<img class="img-lazyload" src="<?php echo __ROOT__?>/<?php echo $v['big']?>" style="left: -350px; opacity: 1;"> 
									</div> 
								</div>
								<?php }?>
								
							</div>
						</div>
						<!--模块列表图片区域  结束-->
						
						<!--尺寸说明 开始-->
						<div id="J_Graphic_size_info">
							<div class="panel-title"> <h1>尺码说明</h1> </div>
							
							<div class="graphic-block">  
								
							<!-- 描述 -->  
							<div class="graphic-text">手工平铺测量有误差，介意慎拍</div>  
								<!-- 表格 -->  
								<table class="size-table"> 
									<thead> 
										<tr>  
											<th>尺码</th>  
											<th>衣长</th>  
											<th>袖长</th>  
											<th>胸围</th>  
											<th>肩宽</th>  
										</tr> 
									</thead> 
									<tbody>  
										<tr>  
											<td>均码</td>  
											<td>105</td>  
											<td>57</td>  
											<td>106</td>  
											<td>41</td>  
										</tr>  
									</tbody> 
								</table>     
								<!-- 提醒 -->  
								<div class="size-text">※ 以上尺寸为实物人工测量，因测量当时不同会有1-2cm误差，相关数据仅作参考，以收到实物为准。</div>  
							</div>
						</div>
						<!--尺寸说明 结束-->
					</div>
					<!--图文详情 结束-->
				</div>
				<!--商品详情  结束-->
				
				<!--累计评价 开始-->	
				<div class="panel-box hidden">
					<div class="module-panel module-rates">
						<div id="J_RatesBuyer">
							<!--标题-->
							<div class="panel-title">
		        					<h1>买家评价</h1>
		    				</div>
		    				
		    				<!--评论区域-->
		    				<div class="rates-buyer">
		    					<!--评论分数-->
		    					<div class="comment-info">
		    						<ul>
		    							<li class="score">
                                        	<span class="comment-star"><b style="width: 77px;"></b></span>
                    						<span class="numbox">
                        						<b class="num-v">4.9</b><span class="num-s">分</span>
                    						</span>
                						</li>
                						<li>
                                            <div class="title">描述相符：</div>
						                    <div class="cont">
						                        <span class="comment-star"><b style="width:77px;"></b></span>
						                        <span class="num-v">4.9</span>
						                    </div>
						                </li>
						                <li>
                                            <div class="title">价格合理：</div>
						                    <div class="cont">
						                        <span class="comment-star"><b style="width:77px;"></b></span>
						                        <span class="num-v">4.9</span>
						                    </div>
						                </li>
						                <li>
                                            <div class="title">质量满意：</div>
						                    <div class="cont">
						                        <span class="comment-star"><b style="width:77px;"></b></span>
						                        <span class="num-v">4.9</span>
						                    </div>
						                </li>
		    						</ul>
		    					</div>
		    					<!--评论分数 结束-->
		    					
		    					<!--评论内容 开始-->
		    					<div class="comment-content">
		    						<!--标签 开始-->
		    						<div class="tags clea">
                    					<div class="list open">
                                        	<a href="javascript:;" class="best">款式好 (4)</a>
                        					<b>|</b>
                                            <a href="javascript:;" class="best">质量很好 (4)</a>
                                			<b>|</b>
                                            <a href="javascript:;" class="best">性价比高 (3)</a>
                                			<b>|</b>
                                        </div>
                                    </div>
		    						<!--标签 结束-->
		    						
		    						<!--导航 开始-->
		    						<div class="nav">
                                        <div class="comment-sort">
	                            			<input class="J_CommentSort" checked="" type="radio"><label for="J_CommentSortDefault"> 默认排序</label>
	                            			<input class="J_CommentSort ml15" name="commentSort" type="radio"><label for="J_CommentSortTime"> 时间排序</label>
                        				</div>
                						<a href="javascript:;" data-type="all" class="c">全部评价（134）</a>
						            </div>
						            <!--导航 结束-->
						            
						            <!--评论列表 开始-->
						            <div id="J_RatesBuyerList" class="comments">
						            	<div class="item">
						            		<!--内容-->
						            		<div class="info clearfloat">
						            			<div class="info-w">
						            				<div class="info-t">
                                    					<span class="name">隔****我</span>
                                       					<span class="date">2015年11月17日</span>
            										</div>
            										<div class="info-m">
														包包很轻，PU有点硬。上网查了条码，应该不是正品哦。只不过包包款式还是可以的啦，有型。                
            										</div>
            										<div class="info-b">
                                                        <span>规格:小号</span>
                                                    </div>
                                                    <div class="info-l"></div>
						            			</div>
						            		</div>
						            		<!--头像-->
						            		<div class="face">
					                            <img src="<?php echo __PUBLIC__?>/Home/images/img_64x64.jpg">
						                    </div>
						            	</div>
						            </div>
						            <!--评论列表 结束-->
						            
						            <!--评论列表 开始-->
						            <div id="J_RatesBuyerList" class="comments">
						            	<div class="item">
						            		<!--内容-->
						            		<div class="info clearfloat">
						            			<div class="info-w">
						            				<div class="info-t">
                                    					<span class="name">隔****我</span>
                                       					<span class="date">2015年11月17日</span>
            										</div>
            										<div class="info-m">
														包包很轻，PU有点硬。上网查了条码，应该不是正品哦。只不过包包款式还是可以的啦，有型。                
            										</div>
            										<div class="info-b">
                                                        <span>规格:小号</span>
                                                    </div>
                                                    <div class="info-l"></div>
						            			</div>
						            		</div>
						            		<!--头像-->
						            		<div class="face">
					                            <img src="<?php echo __PUBLIC__?>/Home/images/img_64x64.jpg">
						                    </div>
						            	</div>
						            </div>
						            <!--评论列表 结束-->
						            
						            <!--评论分页区域 开始-->
						            <div class="pages clearfloat">
						            	<a class="c">1</a>
						            	<a href="">2</a>
						            	<a href="">3</a>
						            	<a href="">4</a>
						            	<a href="">5</a>
						            	<a href="">下一页></a>
						            </div>
						            <!--评论分页区域 结束-->
		    					</div>
		    					<!--评论内容 结束-->
		    				</div>
		    				<!--评论区域 结束-->
						</div>
					</div>
				</div>
				<!--累计评价 结束-->	
				
				<!--选项页  结束-->
			</div>
			<!--商品详情 累计评价 中间区域 结束-->
			
			<!--最右侧区域 开始-->
			<div class="col-extra">
				<!--购物车模块 标题-->
				<div class="module-cart">
		           <a href="javascript:;" class="cart-name">
		           		<span style="padding:0px;padding-right:10px;">商品介绍</span>
		           </a>
        		</div>
				<!--购物车模块 标题 结束-->
				
				<!--子导航列表 开始 -->
				<div class="extranav-bd" id="extranav-bd">
					<ul class="extranav-list">  
						<!-- 商品描述 -->  
						<li class="selected"> 
							<a href="javascript:;">商品描述</a> 
						</li>  
						<!-- 产品参数 -->  
						<li class=""> <a  href="javascript:;">产品参数</a> </li>  
						<!-- 模块列表 -->  
						<li class=""> 
							<a href="javascript:;">穿着效果</a> 
						</li>  
						<li class="" data-module="J_Graphic_size_info"> 
							<a href="javascript:;">尺码说明</a> 
						</li>    <!-- 本店同类商品 --> 
						
					</ul>
				</div>
				<!--子导航列表 结束-->
			</div>
			<!--最右侧区域 结束-->
		</div>
		<!--商品详情展示 区域  结束-->
		<!--空白盒子-->
		<div class="kong clearfloat"></div>
		
		<!--底部公共部分   开始-->
		<!--载入尾部-->
		<!--底部公共部分   开始-->
		<!--底部 开始-->
		<div class="floor-area clearfloat" style="margin-bottom: 10px;">
			<div class="floor">
				<div class="foot-info">
		            <a class="info-logo" href="#"></a>
		            <div class="info-text">
		                <p>站点名称：<a href="" target="_blank"><?php echo C('webset.webname')?></a></p>
		               	<p class="mgjhostname" title="guomai31072"><?php echo C('webset.webdes')?></p>
		            </div>
    			</div>
    			
				<div class="foot_link">
		            <dl class="link_company">
		                <dt>友情链接</dt>
		                <?php foreach ($linkData as $v){?>
		                <dd><a href="<?php echo $v['url']?>" target="_blank"><?php echo $v['lname']?></a></dd>
		                <?php }?>
		            </dl>
		          
        		</div>
			</div>
		<!-- 	<?php if( 'Details'=='Index' && 'index'=='index' ){?>
                
			<div class="w-links clearfloat">
        			<ul>
	            		<li>友情链接: </li>
	                    <li><a target="_blank" href="#">淘粉吧</a></li>
	                    <li><a target="_blank" href="#">蘑菇街团购网</a></li>
	                    <li><a target="_blank" href="#">蘑菇街女装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街男装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街鞋子</a></li>
	                    <li><a target="_blank" href="#">蘑菇街包包</a></li>
	                    <li><a target="_blank" href="#">蘑菇街家居</a></li>
	                    <li><a target="_blank" href="#">家具网</a></li>
	                    <li><a target="_blank" href="#">时尚品牌网</a></li>
	                    <li><a target="_blank" href="#">装修</a></li>
	                    <li><a target="_blank" href="#">蘑菇街母婴</a></li>
                	</ul>
    		</div>
    		
               <?php }?> -->
    		
		</div>
		<!--底部 结束-->
		<!--底部公共部分   结束-->
		
		<?php if( 'Details'=='Index' && 'index'=='index' ){?>
                
		<!--首页头部 弹出搜索框 开始-->
		<div class="sticky-search-container">
			<div class="fix-warp">
				<a href="#" class="logo" title="蘑菇街|我的买手街">蘑菇街|我的买手街</a>
				<div class="nav_search_form">
					<div class="search_inner_box">
						<div class="selectbox">
							<span class="selected">搜商品</span>
						</div>
						<form action="H_S.html" method="get" id="top_nav_form">
                            <input class="fx-txt" value="<?php echo isset($_GET['words'])?$_GET['words']:'裙子'?>"  type="text" name="words">
							<input value="搜  索" class="fx_btn"  type="submit">
            			</form>
					</div>
				</div>
			</div>
		</div>
		<!--首页头部 弹出搜索框 结束-->
		
               <?php }?>
		
		<!--用户信息除外-->
		<?php if( 'Details'!='Userinfo' && 'Details'!='Cart' && 'index'!='userinfo' ){?>
                
		<!--右侧购物车 回到顶部 相对定位 开始-->
		<div class="mgj_rightbar">
			<!--用户头像-->
			<div class="sidebar-item mgj-my-avatar">
				<a href="javascript:;" id="mg-userFace" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>">
					<?php if(isset($_SESSION['uid'])){?>
                
					<div class="img">
						<img src="<?php echo __ROOT__?>/<?php echo $face?>"  height="20" width="20">
					</div>
					<?php }else{?>
					<div class="img">
						<img src="<?php echo __PUBLIC__?>/Home/images/02.jpg_48x48.jpg" height="20" width="20">
					</div>
					
               <?php }?>
				</a>
			</div>
			<!--购物车-->
			<div class="sidebar-item mgj-my-cart" id="mgj-my-cart">
        		<a data-ptp-idx="2" target="_blank" href="<?php echo __ROOT__?>/H_C_i.html">
            		<i class="s-icon"></i>
            		<div class="s-txt">购物车</div>
            		<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
            		<div class="num floorNum"><?php echo $_SESSION['tatol']?></div>
            		
               <?php }?>
        		</a>
        		
        		<!--隐藏购物车div 开始-->
        		<div class="cart-hide-area hidden" id="cart-hide-area">
        			<p class="p1">
        				<span class="mac-success-txt module-cart-icons">已将商品添加到购物车</span>
        			</p>
        			<p><a href="<?php echo __ROOT__?>/H_C_i.html" class="mac-go-cart module-cart-icons">去购物车结算</a></p>
        			<a href="javascript:;" class="J_Close fix-close-btn">关闭</a>
        			<span class="sanxiao"></span>
        		</div>
        		
        		<!--隐藏购物车div 结束-->
    		</div>
    		<!--回到顶部-->
    		<div class="sideBottom">
       			<div class="sidebar-item mgj-back2top">
		            <a rel="nofollow" href="javascript:;">
		                <i class="s-icon"></i>
		            </a>
		        </div>
    		</div>
		</div>
		<!--右侧购物车 回到顶部 相对定位 结束-->
		
               <?php }?>
		
	</body>
<!-- 载入JS区域 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/jquery-1.8.3.min.js" ></script>

<!-- 载入HDjs -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>
<!-- 载入城市联动 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/city.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/formValidate.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/index.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/common.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/slide.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/goods.js"></script>




<!-- 载入Uploadify上传插件 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/method.js"></script>

</html>

	