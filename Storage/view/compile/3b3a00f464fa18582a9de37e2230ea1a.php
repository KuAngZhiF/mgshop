<!--载入头部-->
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>后台管理系统</title>
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/style.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/node.css" />


<link rel="stylesheet" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css">

<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/uniform.default.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/jquery.fancybox.css">

<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">

<!-- 载入文本编译器 -->
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript">
	var Public = "<?php echo __PUBLIC__?>";
	var Root = "<?php echo __ROOT__?>";
	var picUploadUrl = "<?php echo U('Lists/picUploads')?>";
	var MiniUploadUrl = "<?php echo U('Lists/MiniUploads')?>";
	var sname = "<?php echo session_name();?>";
	var sid = "<?php echo session_id();?>";
	var FaceUrl = "<?php echo U('System/face')?>";
	var imgsGoodsUrl = "<?php echo U('Goods/imgsGoods')?>";
	var CateMiniUrl = "<?php echo U('Lists/CateMiniUp')?>";
	var CateMaxUrl = "<?php echo U('Lists/CateMaxUpOne')?>";
	var CateMaxtwoUrl = "<?php echo U('Lists/CateMaxUpTwo')?>";
	var CateDelUrl = "<?php echo U('Lists/CateAjaxDel')?>";
	var cateImgDelUrl = "<?php echo U('Goods/cateImgDel')?>";
</script>

</head>
<body>
<div class="topbar clearfix">
	<div class="container-fluid">
		<a href="#" class='company'>MgShop后台管理系统</a>
		<ul class='mini'>
			<li>
				<a href="/" target="_blank">
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/gear.png" alt="">
					查看网站
				</a>
			</li>		
					
			<li>
				<a href="<?php echo U('System/systemSet')?>">
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/gear.png" alt="">
					网站设置
				</a>
			</li>		
			<li>
				<a href="<?php echo U('Login/loginOut')?>" id='loginOut'>
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/control-power.png" alt="">
					退出系统
				</a>
			</li>
		</ul>
	</div>
</div>
<!--面包屑-->
<div class="breadcrumbs">
	<div class="container-fluid">
		<ul class="bread pull-left">
			<li>
				<a href="dashboard.html"><i class="icon-home icon-white"></i></a>
			</li>
			<li>
				<a href="dashboard.html">
					
				</a>
			</li>
		</ul>

	</div>
</div>
<!--面包屑结束-->

<script type="text/javascript">
		var changeSpce = "<?php echo U('Lists/changeSpce')?>";
</script>
<div class="main">
	<div class="container-fluid">
	<!--左边的导航-->
	<div class="navi">
		<ul class='main-nav'>
			<li <?php if( 'Goods'=='Index'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-home icon-white"></i></div>
					控制面板
					<?php if( 'Goods'=='Index' ){?>
                
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-up-white.png" alt="">
					<?php }else{?>
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
					
               <?php }?>					
				</a>
				
				<ul <?php if( 'Goods'=='Index'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Index' && 'goodsListAdd'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Index/index')?>">
							后台首页
						</a>
					</li>
				</ul>
			</li>
			<li <?php if( 'Goods'=='Goods'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					商品管理
					<?php if( 'Goods'=='Goods' ){?>
                
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-up-white.png" alt="">
					<?php }else{?>
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
					
               <?php }?>
				</a>
				<ul <?php if( 'Goods'=='Goods'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Goods' && 'goodsListAdd'=='listType' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listType')?>">
							类型管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'goodsListAdd'=='ListCategory' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/ListCategory')?>">
							分类管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'goodsListAdd'=='CategoryImg' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/CategoryImg')?>">
							分类图片管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'goodsListAdd'=='cateImgList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/cateImgList')?>">
							栏目图片管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'goodsListAdd'=='listBrand' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listBrand')?>">
							品牌管理
						</a>
					</li>
					
					<li <?php if( 'Goods'=='Goods' && 'goodsListAdd'=='listGoods' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listGoods')?>">
							商品管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'goodsListAdd'=='recycle' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/recycle')?>">
							回收站
						</a>
					</li>
				</ul>
			</li>
			
			<li <?php if( 'Goods'=='Order'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					订单管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Order'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Order' && 'goodsListAdd'=='orderList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Order/orderList')?>">
							订单列表
						</a>
					</li>
					
				</ul>
			</li>
			
			<li <?php if( 'Goods'=='Pay'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					充值管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Pay'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Pay' && 'goodsListAdd'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Pay/index')?>">
							充值管理
						</a>
					</li>
					
				</ul>
			</li>

			<!-- <li >
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-book icon-white"></i></div>
					评论管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul class='collapsed-nav closed'>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							评论列表
						</a>
					</li>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							检索评论
						</a>
					</li>					
				</ul>
			</li> -->
			


			<li <?php if( 'Goods'=='System'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-exclamation-sign icon-white"></i></div>
					系统管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='System'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='System' && 'goodsListAdd'=='linkList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/linkList')?>">
							友情链接
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'goodsListAdd'=='UserFace' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/UserFace')?>">
							修改头像
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'goodsListAdd'=='editUser' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/editUser')?>">
							修改密码
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'goodsListAdd'=='systemSet' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/systemSet')?>">
							网站设置
						</a>
					</li>
				</ul>
			</li>
		
			

			 <li <?php if( 'Goods'=='Rbac'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-tasks icon-white"></i></div>
					RBAC权限管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Rbac'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Rbac' && 'goodsListAdd'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/index')?>">
							用户列表
						</a>
					</li>
					
					<li <?php if( 'Goods'=='Rbac' && 'goodsListAdd'=='Rolelist' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/Rolelist')?>">
							角色列表
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'goodsListAdd'=='addRole' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/addRole')?>">
							添加角色
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'goodsListAdd'=='nodelist' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/nodelist')?>">
							节点列表
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'goodsListAdd'=='addNode' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/addNode')?>">
							添加节点
						</a>
					</li>
				</ul>
			</li>
	

			
		</ul>
	</div>
	<!--左边的导航 结束-->

	<div class="content">
			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-head" >
							<h3 ><button class="btn btn-info"><?php if($glid){?>
                修改<?php }else{?>添加
               <?php }?>货品</button></h3>
							<div style="float:right;margin:5px; cursor:pointer;" onclick='location.href="<?php echo U('goodsList',array('gid'=>$_GET['gid']))?>"'>返回到上一级&nbsp<img src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/16/refresh.png"  title="返回到上一级" ></div>
						</div>
						<div class="box-content">
							<form action="" class="form-horizontal" method="post" id="goodsListAddForm">
									
									<?php foreach ($NormsData as $gtname=>$v){?>
									<div class="control-group">
										<label for="select" class="control-label"><?php echo $gtname?></label>
										<div class="controls">
											<select name="spce[]" style="padding-left:5px;width:224px;" class="spceSize">
												<option value="0" >请选择...</option>
												<?php foreach ($v as $vo){?>
												<option value="<?php echo $vo['gt_id']?>" gid="<?php echo $_GET['gid']?>" <?php if(in_array($vo['gt_id'],$combine)){?>
                selected
               <?php }?>><?php echo $vo['gt_name']?></option>
												<?php }?>
											</select>
										</div>
									</div>
									<?php }?>
									
									<!-- <div class="control-group">
										<label for="goods_name" class="control-label">商品货号</label>
										<div class="controls">
											<input type="text" name="number"  class='input-square' value="<?php echo isset($oldData['number']) ? $oldData['number'] :''?>">
										</div>
									</div> -->
									
									<div class="control-group">
										<label for="goods_name" class="control-label">库存</label>
										<div class="controls">
											<input type="text" name="inventory"  class='input-square' value="<?php echo isset($oldData['inventory']) ? $oldData['inventory'] :''?>">
										</div>
									</div>
								
									<div class="form-actions">
										<input type="hidden" name="goods_gid" value="<?php echo $_GET['gid']?>">
										<input type="hidden" name="glid" value="<?php echo isset($_GET['glid'])?$_GET['glid']:0?>">
										<button class="btn btn-primary" type="submit">提交</button>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>

		
		</div>	
	</div>
</div>	

<!--载入尾部-->
<script src="<?php echo __PUBLIC__?>/Admin/js/jquery.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/common.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/goods.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/formValidate.js"></script>
<script src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>

<script src="<?php echo __PUBLIC__?>/Admin/js/bootstrap.min.js"></script>


<script src="<?php echo __PUBLIC__?>/Admin/js/admin.js"></script>
<!-- <script src="<?php echo __PUBLIC__?>/Admin/My97DatePicker/WdatePicker.js"></script> -->
<script src="<?php echo __PUBLIC__?>/Admin/js/jquery.fancybox.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/js/custom.js"></script>


</body>
</html>
