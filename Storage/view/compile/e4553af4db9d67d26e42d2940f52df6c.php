<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>蘑菇街-订单详情</title>
		<!--载入头部-->
				<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/common.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/floor.css"/>
		
		<!-- 载入HDjs样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css"/>
		<!-- 首页样式只有首页有 -->
		<?php if( 'Userinfo'=='Index' && 'orderdetail'=='index' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/index.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Userinfo' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/userinfo.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Lists' ){?>
                
		<!-- 列表页样式  -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/lists.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Details' ){?>
                
		<!-- 详情页 样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/details.css"/>
		
               <?php }?>
		
		<!-- 购物车样式 -->
		<?php if( 'Userinfo'=='Cart' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/cart.css"/>
		
               <?php }?>
		
		<?php if( 'Userinfo'=='Userinfo' && 'orderdetail'=='orderdetail' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/indent.css"/>
		
               <?php }?>
		
		<!-- 搜索页样式 -->
		<?php if( 'Userinfo'=='Search' ){?>
                
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Home/css/seek.css" />
		
               <?php }?>
		
		<!-- 载入上传Uploadify样式 -->
		<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">
		
		<script type="text/javascript">
				var Public = "<?php echo __PUBLIC__?>";
				var Root = "<?php echo __ROOT__?>";
				var sname = "<?php echo session_name();?>";
				var sid = "<?php echo session_id();?>";
				var userFaceUrl = "<?php echo U('Userinfo/face')?>";
		</script>
	</head>
	<body>
		<!--公共头部样式 开始-->
		<!--头部 开始-->
		<div class="header-area">
			<div class="header">
				<a href="<?php echo __ROOT__?>" class="home">蘑菇街首页</a>
				<ul class="header-top">
					<!--用户名登录后 的div-->
					<?php if(isset($_SESSION['uid'])?$_SESSION['uid']:0){?>
                
					<li class="t1 has_icon user_meta" id="user_meta">
	                    <a href="javascript:;">很纯很暧昧777</a>
	                    <a href="" target="_blank"><span class="user-level user-level0">&nbsp;</span></a>
	                    <i class="icon_delta"></i>
	                    <ol class="ext_mode" id="menu_personal" style="display: none;">
	                        <li class="s2"><a target="_blank" href="H_U.html">个人设置</a></li>
	                        <li class="s2"><a target="_blank" href="H_U.html">账号绑定</a></li>
	                        <li class="s2"><a rel="nofollow" href="H_logout.html">退出</a></li>
	                    </ol>
                	</li>
                	<!--用户名登录后 的div 结束-->
                	<?php }else{?>
                	<!--没登录状态-->
					<li class="t1"><a href="H_reg.html">注册</a></li>
					<li class="t1"><a href="H_log.html">登录</a></li>
					<!--没登录状态 结束-->
					
               <?php }?>
					<li class="t1 myorder t1-line" id="J-order" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>"><a href="javascript:;">我的订单</a></li>
					<li class="t1 myorder t1-cate" id="t1-cate">
						<a href="H_C_i.html">购物车
							<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
							<span class="floorNum" style="color: #f36;padding: 0px;font-weight: 400;"><?php echo $_SESSION['tatol']?></span>
							<span>件</span>
							
               <?php }?>
						</a>
						<!--购物车隐藏盒子 开始-->
						
						<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
						<!--有商品的时候-->
						<div class="cate-hide cate-info" style="display: none;">
							<ul>
								<?php foreach ($_SESSION['goods'] as $v){?>
	                            <li>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="imgbox">
										<img src="<?php echo __ROOT__?>/<?php echo $v['pic']?>"  width="45" height="45">
									</a>
									<a href="H_D_i_<?php echo $v['gid']?>.html" target="_blank" class="title"><?php echo $v['gname']?></a>
									<span class="info">
											<?php foreach ($v['options'] as $gtname=>$vo){?>
									    	<span><?php echo $gtname?>：<?php echo $vo?></span>
									    	<?php }?>
									</span>
									<span class="price">￥<?php echo $v['shopprice']?></span>
									<a href="javascript:;" class="del delete" mgprice="<?php echo $v['price']?>" glid="<?php echo $v['glid']?>">删除</a>
								</li>
								<?php }?>
								
							</ul>
							<div class="subbox">
                    			<div class="fr">
                				<a href="H_C_i.html" target="_blank" class="goel">查看购物车</a>
            					</div>
        					</div>
						</div>
						<!--购物车隐藏盒子 结束-->
						<?php }else{?>
						<!--没有商品的时候-->
						<div class="cate-hide empty_cart" style="display: none;">
							购物车里没有商品！
						</div>
						<!--没有商品的时候 结束-->
						
               <?php }?>
					</li>
				</ul>
				
			</div>
		</div>
		<!--头部 结束-->
		
		<!--搜索区域   开始-->
		<div class="search-warp">
			<div class="search-area">
			<div class="logo">
				<a href="<?php echo __ROOT__?>" title="蘑菇街首页"></a>
			</div>
			
			<!--搜索框 区域-->
			<div class="search">
				<div class="search-box">
					<div class="selectbox">
                		<span class="selected">搜商品</span>
               		</div>
               		<!--form 表单 开始-->
               		<form action="H_S.html" method="get" id="sea-form">
               			<input type="text" value="上衣" class="ts-txt" name="words"/>
               			<input type="submit" value="搜  索" class="ts-btn"/>
               		</form>
               		<!--form 表单 结束-->
               	</div>
               	
               	<!--栏目列表  开始-->
               	<div class="cate-list">
               		<a href="">呢外套</a>
               		<a href="">风衣</a>
               		<a href="">羽绒服</a>
               		<a href="">运动鞋</a>
               		<a href="">卫衣</a>
               		<a href="">雪地靴</a>
               		<a href="">家居服</a>
               		<a href="">新款女包</a>
               		<a href="">毛衣</a>
               	</div>
               	<!--栏目列表 结束-->
				
			</div>
			<!--搜索框 结束-->
			
			<!--二维码 开始-->
	       		<div class="e-qrcode">
	                <img src="<?php echo __PUBLIC__?>/Home/images/ecode.png" alt="蘑菇街客户端下载" height="70" width="70">
	                <p>蘑菇街客户端</p>
	            </div>
       		<!--结束-->
		</div>
		</div>
		<!--搜索区域   结束-->
		
		<?php if( 'Userinfo'=='Index' && 'orderdetail'=='index' ){?>
                
		<!--菜单列表 开始-->
		<div class="menu-area">
			<div class="menu">
				<ul class="menu-list">
					<li class="home">全部商品</li>
									<?php
					$model = new \Common\Model\Category();
					$data = $model->where('pid=0 AND is_show=1')->limit(8)->get();
					foreach($data as $field):
					//列表页地址
					$field['url'] =__ROOT__ .  "/H_L_i_". $field['cid'] . ".html";
				?>
					
					<li><a href="<?php echo $field['url']?>"><?php echo $field['cname']?></a></li>
					
				<?php endforeach;?>
				</ul>
			</div>
		</div>
		<!--菜单列表 结束-->
		
               <?php }?>
		
		<!--公共头部样式 结束-->
		
		<!--下滑线2px-->
		<div class="overstriking"></div>
		
		<!--公共头部样式 结束-->
		
		<!--主体内容  订单 详情 开始-->
		
		<div class="muorder">
			<div class="mu-warp">
				<h2 class="mu-head">订单详情</h2>
				<!--订单时间 开始-->
				<div class="mu_d_lines">
		            <span class="mw">订单编号: <?php echo $orderData['number']?></span>
		            <span class="mw">当前状态: 
		            <?php if($orderData['status']==1){?>
                
		            <span class="cancel">等待付款</span>
		            <?php }else if($orderData['status']==2){?>
		             <span class="cancel">等待卖家发货</span>
		             <?php }else if($orderData['status']==3){?>
		              <span class="cancel">卖家已发货</span>
		              <?php }else{?>
		               <span class="cancel">交易完成</span>
		               
               <?php }?>
		            </span>
        		</div>
				<!--订单时间 结束-->
				<!--订单进度 开始-->
				<div class="md_process">
					<div <?php if($orderData['status']==1){?>
                class="md_process_wrap md_process_step1" <?php }else if($orderData['status']==2){?>class="md_process_wrap md_process_step2"<?php }else if($orderData['status']==3){?>class="md_process_wrap md_process_step3"<?php }else if($orderData['status']==4){?>class="md_process_wrap md_process_step4"<?php }else{?>class="md_process_wrap md_process_step5"
               <?php }?>>
        			<!-- class: step样式不加为全黑，md_process_step1 为第一步，依次类推 -->
        			<div class="md_process_sd"></div>
	        			<i class="md_process_i md_process_i1">1
	            		<span class="md_process_tip">提交订单</span>
	        				<span class="md_process_tip_bt"><?php echo date('Y-m-d H:i:s',$orderData['time'])?></span>
	        			</i>
						<i class="md_process_i md_process_i2">2
	            			<span class="md_process_tip">买家支付</span>
	            			<?php if($orderData['plytime']){?>
                
	            			<span class="md_process_tip_bt"><?php echo date('Y-m-d H:i:s',$orderData['plytime'])?></span>
	            			
               <?php }?>
	                    </i>
						<i class="md_process_i md_process_i3">3
	            			<span class="md_process_tip">卖家发货</span>
	            			<?php if($orderData['sendtime']){?>
                
	            			<span class="md_process_tip_bt"><?php echo date('Y-m-d H:i:s',$orderData['sendtime'])?></span>
	                    	
               <?php }?>
	                    </i>
						<i class="md_process_i md_process_i4">4
	            			<span class="md_process_tip">确认收货</span>
	            			<?php if($orderData['puttime']){?>
                
	            			<span class="md_process_tip_bt"><?php echo date('Y-m-d H:i:s',$orderData['puttime'])?></span>
	                   		
               <?php }?>
	                    </i>
						<i class="md_process_i md_process_i5">5
	            			<span class="md_process_tip">评价</span>
	                    </i>
    				</div>
				</div>
				<!--订单进度 结束-->
				
				<!--详细信息 开始-->
				<div class="mu_d_info">
        			<h5 class="mu_d_info_tit">详细信息</h5>
					<dl class="mu_d_infolist">
						<dt>收 货 人：</dt>
						<dd><?php echo $orderData['consignee']?></dd>
						<dt>收货地址：</dt>
						<dd><?php echo $orderData['address']?></dd>
						<dt>联系电话：</dt>
						<dd><?php echo $orderData['mobile']?></dd>
					</dl>
				</div>
				<!--详细信息 结束-->
				
				
				<!--物流信息 开始-->
				<?php if($LogData){?>
                
				<div class="mu_d_info">
        			<h5 class="mu_d_info_tit">物流信息</h5>
					<dl class="mu_d_infolist">
						<dt>运单号码：</dt>
						<dd><?php echo $LogData['waybill']?></dd>
						<dt>物流公司：</dt>
						<dd><?php echo $LogData['company']?></dd>
						<dt>发货地址：</dt>
						<dd>
							<span><?php echo $LogData['address']?></span>
							<span style="font-weight: 700;margin-right: 10px;color: #333;"><?php echo $LogData['zipcode']?></span>
							<span style="font-weight: 700;margin-right: 10px;color: #333;"><?php echo $LogData['logname']?></span>
							<span style="font-weight: 700;margin-right: 10px;color: #333;"><?php echo $LogData['cellphone']?></span>
						</dd>
						<dt>客服电话：</dt>
						<dd><?php echo $LogData['tel']?></dd>
					</dl>
				</div>
				<!--物流信息 结束-->
				
               <?php }?>
				
				<!--订单信息 开始-->
				<div class="mu_d_orderlist">
					<h5 class="mu_d_info_tit">商品清单</h5>
					<ul class="mo_orderlist">
						<li class="mo_orderitem">
							<!--标题 开始-->
							<ul class="mo_orderitem_thlist clearfloat">
					            <li class="td_goods">商品</li>
					            <li class="td_price">单价（元）</li>
					            <li class="td_count">数量</li>
					            <li class="td_wipay">交易状态</li>
					           	<li class="td_total">订单额（元）</li>
        					</ul>
							<!--标题 结束-->
							<!--信息 开始-->
							<ul class="mo_orderitem_det clearfloat">
								<!--商品 单元 数量  开始-->
								<li class="td_colrow">
									<ul class="td_colrow_wrap clearfloat">
										<li class="td_goods">
											<div class="td_wrap">
                            					<a href="H_D_i_<?php echo $orderData['goods_gid']?>.html" target="_blank">
                            						<img class="mo_orderitem_gdimg" src="<?php echo __ROOT__?>/<?php echo $orderData['pic']?>"  height="60" width="60">
                            					</a>
                            					<p class="mo_orderitem_h mo_orderitem_gdinfo mb4">
                            						<a href="H_D_i_<?php echo $orderData['goods_gid']?>.html" target="_blank"><?php echo msubstr($orderData['gname'],0,16)?></a>
                            					</p>
                            					<?php foreach ($orderData['options'] as $gtname=>$gt_name){?>
												<p class="mo_orderitem_d mo_orderitem_gdinfo"><?php echo $gtname?>：<?php echo $gt_name?></p>
					                          	<?php }?>
					                        </div>
										</li>
										<li class="td_price">
											<div class="td_wrap">
                            					<p class="mu_money mu_org_money">¥ <?php echo $orderData['marketprice']?></p>
                                                <p class="mu_money">¥ <?php echo $orderData['shopprice']?></p>
                                            </div>
										</li>
										<li class="td_count">
                        					<div class="td_wrap"><?php echo $orderData['quantity']?></div>
                    					</li>
									</ul>
								</li>
								<!--商品 单元 数量  结束-->
								<li class="td_wipay">
                					<div class="td_wrap">
                    					 <?php if($orderData['status']==1){?>
                
							            <span class="cancel">订单未付款</span>
							            <?php }else if($orderData['status']==2){?>
							             <span class="cancel">卖家待发货</span>
							             <?php }else if($orderData['status']==3){?>
							              <span class="cancel">卖家已发货</span>
							              <?php }else{?>
							               <span class="cancel">交易完成</span>
							              
               <?php }?>               
                					</div>
            					</li>
								<li class="td_total">
                					<div class="td_wrap">
                    				<span class="mo_orderitem_money_gray mu_money">¥ <?php echo $orderData['subtotal']?></span>
                                    </div>
            					</li>
							</ul>
							<!--信息 结束-->
						</li>
					</ul>
				</div>
				
			</div>
			<!--订单信息 结束-->
			
		</div>
		
		<!--主体内容  订单 详情 开始-->
		
		
		<!--底部公共部分   开始-->
		<!--载入尾部-->
		<!--底部公共部分   开始-->
		<!--底部 开始-->
		<div class="floor-area clearfloat">
			<div class="floor">
				<div class="foot-info">
		            <a class="info-logo" href="#"></a>
		            <div class="info-text">
		                <p>营业执照注册号：<a href="" target="_blank">330106000129004</a></p>
		                <p>增值电信业务经营许可证：<a href="" target="_blank">浙B2-20110349</a></p>
		                <p>ICP备案号：浙ICP备10044327号-3</p>
		                <p class="mgjhostname" title="guomai31072">©2015 Mogujie.com 杭州卷瓜网络有限公司</p>
		            </div>
    			</div>
    			
				<div class="foot_link">
		            <dl class="link_company">
		                <dt>公司</dt>
		                <dd><a href="" target="_blank">关于我们</a></dd>
		                <dd><a href="" target="_blank">招聘信息</a></dd>
		                <dd><a href="" target="_blank">联系我们</a></dd>
		            </dl>
		            <dl class="link_consumer">
		                <dt>消费者</dt>
		                <dd><a href="" target="_blank">帮助中心</a></dd>
		                <dd><a href="" target="_blank">意见反馈</a></dd>
		                <dd><a href="" target="_blank">手机版下载</a></dd>
		            </dl>
		            <dl class="link_business">
		                <dt>商家</dt>
		                <dd><a href="" target="_blank">免费开店</a></dd>
		                <dd><a href="" target="_blank">商家社区</a></dd>
		                <dd><a href="" target="_blank">商家入驻</a></dd>
		                <dd><a href="" target="_blank">管理后台</a></dd>
		               
		            </dl>
		            <dl class="link_safe">
		                <dt>权威认证</dt>
		                <dd>
		                    <a class="pc" target="_blank" href="#"></a>
		                    <a class="pa" target="_blank" href="#"></a>
		                 	<a class="kx" href="javascript:;" target="_blank"></a>
		                </dd>
		            </dl>
        		</div>
				
				
    			
			</div>
			<?php if( 'Userinfo'=='Index' && 'orderdetail'=='index' ){?>
                
			<div class="w-links clearfloat">
        			<ul>
	            		<li>友情链接: </li>
	                    <li><a target="_blank" href="#">淘粉吧</a></li>
	                    <li><a target="_blank" href="#">蘑菇街团购网</a></li>
	                    <li><a target="_blank" href="#">蘑菇街女装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街男装</a></li>
	                    <li><a target="_blank" href="#">蘑菇街鞋子</a></li>
	                    <li><a target="_blank" href="#">蘑菇街包包</a></li>
	                    <li><a target="_blank" href="#">蘑菇街家居</a></li>
	                    <li><a target="_blank" href="#">家具网</a></li>
	                    <li><a target="_blank" href="#">时尚品牌网</a></li>
	                    <li><a target="_blank" href="#">装修</a></li>
	                    <li><a target="_blank" href="#">蘑菇街母婴</a></li>
                	</ul>
    		</div>
    		
               <?php }?>
    		
		</div>
		<!--底部 结束-->
		<!--底部公共部分   结束-->
		
		<?php if( 'Userinfo'=='Index' && 'orderdetail'=='index' ){?>
                
		<!--首页头部 弹出搜索框 开始-->
		<div class="sticky-search-container">
			<div class="fix-warp">
				<a href="#" class="logo" title="蘑菇街|我的买手街">蘑菇街|我的买手街</a>
				<div class="nav_search_form">
					<div class="search_inner_box">
						<div class="selectbox">
							<span class="selected">搜商品</span>
						</div>
						<form action="H_S.html" method="get" id="top_nav_form">
                            <input class="fx-txt" value="风衣"  type="text" name="words">
							<input value="搜  索" class="fx_btn"  type="submit">
            			</form>
					</div>
				</div>
			</div>
		</div>
		<!--首页头部 弹出搜索框 结束-->
		
               <?php }?>
		
		<!--用户信息除外-->
		<?php if( 'Userinfo'!='Userinfo' && 'Userinfo'!='Cart' && 'orderdetail'!='userinfo' ){?>
                
		<!--右侧购物车 回到顶部 相对定位 开始-->
		<div class="mgj_rightbar">
			<!--用户头像-->
			<div class="sidebar-item mgj-my-avatar">
				<a href="javascript:;" id="mg-userFace" uid="<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:0?>">
					<?php if(isset($_SESSION['uid'])){?>
                
					<div class="img">
						<img src="<?php echo __ROOT__?>/<?php echo $face?>"  height="20" width="20">
					</div>
					<?php }else{?>
					<div class="img">
						<img src="<?php echo __PUBLIC__?>/Home/images/02.jpg_48x48.jpg" height="20" width="20">
					</div>
					
               <?php }?>
				</a>
			</div>
			<!--购物车-->
			<div class="sidebar-item mgj-my-cart" id="mgj-my-cart">
        		<a data-ptp-idx="2" target="_blank" href="<?php echo __ROOT__?>/H_C_i.html">
            		<i class="s-icon"></i>
            		<div class="s-txt">购物车</div>
            		<?php if(isset($_SESSION['goods'])?$_SESSION['goods']:'' ){?>
                
            		<div class="num floorNum"><?php echo $_SESSION['tatol']?></div>
            		
               <?php }?>
        		</a>
        		
        		<!--隐藏购物车div 开始-->
        		<div class="cart-hide-area hidden" id="cart-hide-area">
        			<p class="p1">
        				<span class="mac-success-txt module-cart-icons">已将商品添加到购物车</span>
        			</p>
        			<p><a href="<?php echo __ROOT__?>/H_C_i.html" class="mac-go-cart module-cart-icons">去购物车结算</a></p>
        			<a href="javascript:;" class="J_Close fix-close-btn">关闭</a>
        			<span class="sanxiao"></span>
        		</div>
        		
        		<!--隐藏购物车div 结束-->
    		</div>
    		<!--回到顶部-->
    		<div class="sideBottom">
       			<div class="sidebar-item mgj-back2top">
		            <a rel="nofollow" href="javascript:;">
		                <i class="s-icon"></i>
		            </a>
		        </div>
    		</div>
		</div>
		<!--右侧购物车 回到顶部 相对定位 结束-->
		
               <?php }?>
		
	</body>
<!-- 载入JS区域 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/jquery-1.8.3.min.js" ></script>

<!-- 载入HDjs -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>
<!-- 载入城市联动 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/city.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/formValidate.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/index.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/common.js"></script>
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/slide.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/js/goods.js"></script>




<!-- 载入Uploadify上传插件 -->
<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>


<script type="text/javascript" src="<?php echo __PUBLIC__?>/Home/mgjs/method.js"></script>

</html>

	