<!--载入头部-->
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>后台管理系统</title>
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/style.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/node.css" />


<link rel="stylesheet" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css">

<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/uniform.default.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/jquery.fancybox.css">

<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">

<!-- 载入文本编译器 -->
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript">
	var Public = "<?php echo __PUBLIC__?>";
	var Root = "<?php echo __ROOT__?>";
	var picUploadUrl = "<?php echo U('Lists/picUploads')?>";
	var MiniUploadUrl = "<?php echo U('Lists/MiniUploads')?>";
	var sname = "<?php echo session_name();?>";
	var sid = "<?php echo session_id();?>";
	var FaceUrl = "<?php echo U('Index/face')?>";
	var imgsGoodsUrl = "<?php echo U('Goods/imgsGoods')?>";
	var CateMiniUrl = "<?php echo U('Lists/CateMiniUp')?>";
	var CateMaxUrl = "<?php echo U('Lists/CateMaxUpOne')?>";
	var CateMaxtwoUrl = "<?php echo U('Lists/CateMaxUpTwo')?>";
	var CateDelUrl = "<?php echo U('Lists/CateAjaxDel')?>";
	var cateImgDelUrl = "<?php echo U('Goods/cateImgDel')?>";
</script>

</head>
<body>
<div class="topbar clearfix">
	<div class="container-fluid">
		<a href="#" class='company'>MgShop后台管理系统</a>
		<ul class='mini'>
			<li>
				<a href="{:U('Home/Index/index')}" target="_blank">
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/gear.png" alt="">
					查看网站
				</a>
			</li>		
					
			<li>
				<a href="{:U('System/systemset')}">
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/gear.png" alt="">
					网站设置
				</a>
			</li>		
			<li>
				<a href="<?php echo U('Login/loginOut')?>" id='loginOut'>
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/control-power.png" alt="">
					退出系统
				</a>
			</li>
		</ul>
	</div>
</div>
<!--面包屑-->
<div class="breadcrumbs">
	<div class="container-fluid">
		<ul class="bread pull-left">
			<li>
				<a href="dashboard.html"><i class="icon-home icon-white"></i></a>
			</li>
			<li>
				<a href="dashboard.html">
					
				</a>
			</li>
		</ul>

	</div>
</div>
<!--面包屑结束-->

<style type="text/css">
		/*去掉uploadify上传按钮的边框*/
	    .uploadify-button {
	        background-color: transparent;
	        border: none;
	        padding: 0;
	    }
	    .uploadify-button:hover {
	        background-color: transparent;
    	}
 </style> 


<div class="main">
	<div class="container-fluid">
	
	<!--左边的导航-->
	<div class="navi">
		<ul class='main-nav'>
			<li <?php if( 'Goods'=='Index'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-home icon-white"></i></div>
					控制面板
					<?php if( 'Goods'=='Index' ){?>
                
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-up-white.png" alt="">
					<?php }else{?>
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
					
               <?php }?>					
				</a>
				
				<ul <?php if( 'Goods'=='Index'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Index' && 'cateImgShow'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Index/index')?>">
							后台首页
						</a>
					</li>
					<li <?php if( 'Goods'=='Index' && 'cateImgShow'=='UserFace' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Index/UserFace')?>">
							修改头像
						</a>
					</li>
					
				</ul>
			</li>
			<li <?php if( 'Goods'=='Goods'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					商品管理
					<?php if( 'Goods'=='Goods' ){?>
                
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-up-white.png" alt="">
					<?php }else{?>
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
					
               <?php }?>
				</a>
				<ul <?php if( 'Goods'=='Goods'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Goods' && 'cateImgShow'=='listType' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listType')?>">
							类型管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgShow'=='ListCategory' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/ListCategory')?>">
							分类管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgShow'=='CategoryImg' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/CategoryImg')?>">
							分类图片管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgShow'=='cateImgList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/cateImgList')?>">
							栏目图片管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgShow'=='listBrand' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listBrand')?>">
							品牌管理
						</a>
					</li>
					
					<li <?php if( 'Goods'=='Goods' && 'cateImgShow'=='listGoods' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listGoods')?>">
							商品管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgShow'=='recycle' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/recycle')?>">
							回收站
						</a>
					</li>
				</ul>
			</li>
			
			<li <?php if( 'Goods'=='Order'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					订单管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Order'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Order' && 'cateImgShow'=='orderList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Order/orderList')?>">
							订单列表
						</a>
					</li>
					
				</ul>
			</li>
			
			<li <?php if( 'Goods'=='Pay'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					充值管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Pay'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Pay' && 'cateImgShow'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Pay/index')?>">
							充值管理
						</a>
					</li>
					
				</ul>
			</li>

			<li >
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-book icon-white"></i></div>
					评论管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul class='collapsed-nav closed'>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							评论列表
						</a>
					</li>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							检索评论
						</a>
					</li>					
				</ul>
			</li>
			


			<li>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-exclamation-sign icon-white"></i></div>
					系统管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul class='collapsed-nav closed'>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							修改密码
						</a>
					</li>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							网站设置
						</a>
					</li>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							关键字过滤
						</a>
					</li>
					
				</ul>
			</li>
		
			

			 <li <?php if( 'Goods'=='Rbac'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-tasks icon-white"></i></div>
					RBAC权限管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Rbac'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Rbac' && 'cateImgShow'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/index')?>">
							用户列表
						</a>
					</li>
					
					<li <?php if( 'Goods'=='Rbac' && 'cateImgShow'=='Rolelist' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/Rolelist')?>">
							角色列表
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'cateImgShow'=='addRole' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/addRole')?>">
							添加角色
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'cateImgShow'=='nodelist' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/nodelist')?>">
							节点列表
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'cateImgShow'=='addNode' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/addNode')?>">
							添加节点
						</a>
					</li>
				</ul>
			</li>
	

			
		</ul>
	</div>
	<!--左边的导航 结束-->

	<div class="content">
			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-head">
							<h3 ><button class="btn btn-info">修改图片</button></h3>
							<div style="float:right;margin:5px; cursor:pointer;" onclick='location.href="<?php echo U('cateImgList')?>"'>返回到上一级&nbsp<img src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/16/refresh.png"  title="返回到上一级" ></div>
						</div>
						<div class="box-content">
							<form action="<?php echo U('cateImgedit')?>" class="form-horizontal" method="post" id="cateFormOne">
									<div class="control-group">
										<label for="select" class="control-label">所属分类</label>
										<div class="controls">
											<select name="cid"  style="width: 224px;padding-left:5px;" id="cateSelect">
												<option value="">请选择...</option>
												<?php foreach ($cateData as $v){?>
												<option value="<?php echo $v['cid']?>" level="<?php echo $v['_level']?>" <?php if($oldData['cid']==$v['cid']){?>
                selected
               <?php }?>><?php echo $v['_name']?></option>
												<?php }?>
											</select>
										</div>
									</div>
									<?php if($lev==2){?>
                
									<div class="control-group" id="levelTwo" >
										<label for="goods_sn" class="control-label">二级栏目图片</label>
										<div class="controls">
                                            <input type="hidden" name='mini_imgs_120x120' value="<?php echo $oldData['mini_imgs_120x120']?>"  id="cate_mini_imgs"/>
                                            <input type="file" name='cate_mini' id='cate_mini'/>
										</div>
										<!-- 删除按钮 -->
										<div class="controls" id="mini-hide-show" style="position: relative;padding-top:10px;">
                                            <img alt="" src="<?php echo __ROOT__?>/<?php echo $oldData['mini_imgs_120x120']?>"  class="cate-mini-show" style="width:120px;height:120px;">
                                            <a href="javascript:;" class="busyCate" style="display: block;position: absolute;top: -5px;left: 106px;" title="删除图片？" live="1"><img alt="" src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/16/busy.png"></a>
										</div>
									</div>
									
               <?php }?>
									
									<?php if($lev==1){?>
                
									<div class="control-group" id="levelOne1" >
										<label for="goods_sn" class="control-label">一级栏目图片(首页)</label>
										<div class="controls">
                                            <input type="hidden" name='max_imgs_250x130' value="<?php echo $oldData['max_imgs_250x130']?>"  id="cate_max_250x130"/>
                                          	<input type="file" name='cate-max250x130' id='cate-max250x130'/>
										</div>
										<!-- 删除按钮 -->
										<div class="controls" id="max-hide-show1" style="position: relative;padding-top:10px;">
                                            <img alt="" src="<?php echo __ROOT__?>/<?php echo $oldData['max_imgs_250x130']?>"  class="max-img-show1" style="width:250px;height:130px;">
                                            <a href="javascript:;" class="busyCate" style="display: block;position: absolute;top: -5px;left: 237px;" title="删除图片？" live="2"><img alt="" src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/16/busy.png"></a>
										</div>
										<!-- 删除按钮 结束-->
									</div>
									
									<div class="control-group" id="levelOne2" >
										<label for="goods_sn" class="control-label">一级栏目图片(列表)</label>
										<div class="controls">
                                            <input type="hidden" name='max_imgs_200x240' value="<?php echo $oldData['max_imgs_200x240']?>"  id="cate_max_200x240"/>
                                            <input type="file" name='cate-max200x240' id='cate-max200x240'/>
										</div>
										<!-- 删除按钮 -->
										<div class="controls" id="max-hide-show2" style="position: relative;padding-top:10px;">
                                            <img alt="" src="<?php echo __ROOT__?>/<?php echo $oldData['max_imgs_200x240']?>"  class="max-img-show2" style="width:200px;height:240px;">
                                            <a href="javascript:;" class="busyCate" style="display: block;position: absolute;top: -5px;left: 187px;" title="删除图片？" live="3"><img alt="" src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/16/busy.png"></a>
										</div>
										
										<!-- 删除按钮 结束-->
									</div>
									
               <?php }?>
									<div class="form-actions">
										<button class="btn btn-primary" type="submit">提交</button>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>

		
		</div>	
	</div>
</div>

<!--载入尾部-->
<script src="<?php echo __PUBLIC__?>/Admin/js/jquery.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/common.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/goods.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/formValidate.js"></script>
<script src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>

<script src="<?php echo __PUBLIC__?>/Admin/js/bootstrap.min.js"></script>


<script src="<?php echo __PUBLIC__?>/Admin/js/admin.js"></script>
<!-- <script src="<?php echo __PUBLIC__?>/Admin/My97DatePicker/WdatePicker.js"></script> -->
<script src="<?php echo __PUBLIC__?>/Admin/js/jquery.fancybox.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/js/custom.js"></script>


</body>
</html>

