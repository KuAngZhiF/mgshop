<!--载入头部-->
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>后台管理系统</title>
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/style.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/node.css" />


<link rel="stylesheet" href="<?php echo __PUBLIC__?>/hdjs/hdjs.css">

<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/uniform.default.css">
<link rel="stylesheet" href="<?php echo __PUBLIC__?>/Admin/css/jquery.fancybox.css">

<link rel="stylesheet" type="text/css" href="<?php echo __PUBLIC__?>/Uploadify/uploadify.css">

<!-- 载入文本编译器 -->
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="<?php echo __PUBLIC__?>/ueditor1_4_3/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript">
	var Public = "<?php echo __PUBLIC__?>";
	var Root = "<?php echo __ROOT__?>";
	var picUploadUrl = "<?php echo U('Lists/picUploads')?>";
	var MiniUploadUrl = "<?php echo U('Lists/MiniUploads')?>";
	var sname = "<?php echo session_name();?>";
	var sid = "<?php echo session_id();?>";
	var FaceUrl = "<?php echo U('System/face')?>";
	var imgsGoodsUrl = "<?php echo U('Goods/imgsGoods')?>";
	var CateMiniUrl = "<?php echo U('Lists/CateMiniUp')?>";
	var CateMaxUrl = "<?php echo U('Lists/CateMaxUpOne')?>";
	var CateMaxtwoUrl = "<?php echo U('Lists/CateMaxUpTwo')?>";
	var CateDelUrl = "<?php echo U('Lists/CateAjaxDel')?>";
	var cateImgDelUrl = "<?php echo U('Goods/cateImgDel')?>";
</script>

</head>
<body>
<div class="topbar clearfix">
	<div class="container-fluid">
		<a href="#" class='company'>MgShop后台管理系统</a>
		<ul class='mini'>
			<li>
				<a href="/" target="_blank">
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/gear.png" alt="">
					查看网站
				</a>
			</li>		
					
			<li>
				<a href="<?php echo U('System/systemSet')?>">
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/gear.png" alt="">
					网站设置
				</a>
			</li>		
			<li>
				<a href="<?php echo U('Login/loginOut')?>" id='loginOut'>
					<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/control-power.png" alt="">
					退出系统
				</a>
			</li>
		</ul>
	</div>
</div>
<!--面包屑-->
<div class="breadcrumbs">
	<div class="container-fluid">
		<ul class="bread pull-left">
			<li>
				<a href="dashboard.html"><i class="icon-home icon-white"></i></a>
			</li>
			<li>
				<a href="dashboard.html">
					
				</a>
			</li>
		</ul>

	</div>
</div>
<!--面包屑结束-->
<script type="text/javascript">
		var delCateUrl = "<?php echo U('Goods/delCate')?>";
</script>

<div class="main">
	<div class="container-fluid">
	<!--左边的导航-->
	<div class="navi">
		<ul class='main-nav'>
			<li <?php if( 'Goods'=='Index'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-home icon-white"></i></div>
					控制面板
					<?php if( 'Goods'=='Index' ){?>
                
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-up-white.png" alt="">
					<?php }else{?>
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
					
               <?php }?>					
				</a>
				
				<ul <?php if( 'Goods'=='Index'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Index' && 'cateImgList'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Index/index')?>">
							后台首页
						</a>
					</li>
				</ul>
			</li>
			<li <?php if( 'Goods'=='Goods'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					商品管理
					<?php if( 'Goods'=='Goods' ){?>
                
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-up-white.png" alt="">
					<?php }else{?>
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
					
               <?php }?>
				</a>
				<ul <?php if( 'Goods'=='Goods'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Goods' && 'cateImgList'=='listType' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listType')?>">
							类型管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgList'=='ListCategory' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/ListCategory')?>">
							分类管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgList'=='CategoryImg' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/CategoryImg')?>">
							分类图片管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgList'=='cateImgList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/cateImgList')?>">
							栏目图片管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgList'=='listBrand' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listBrand')?>">
							品牌管理
						</a>
					</li>
					
					<li <?php if( 'Goods'=='Goods' && 'cateImgList'=='listGoods' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/listGoods')?>">
							商品管理
						</a>
					</li>
					<li <?php if( 'Goods'=='Goods' && 'cateImgList'=='recycle' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Goods/recycle')?>">
							回收站
						</a>
					</li>
				</ul>
			</li>
			
			<li <?php if( 'Goods'=='Order'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					订单管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Order'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Order' && 'cateImgList'=='orderList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Order/orderList')?>">
							订单列表
						</a>
					</li>
					
				</ul>
			</li>
			
			<li <?php if( 'Goods'=='Pay'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-th-large icon-white"></i></div>
					充值管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Pay'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Pay' && 'cateImgList'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Pay/index')?>">
							充值管理
						</a>
					</li>
					
				</ul>
			</li>

			<!-- <li >
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-book icon-white"></i></div>
					评论管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul class='collapsed-nav closed'>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							评论列表
						</a>
					</li>
					<li class='active'>
						<a href="<?php echo U('Index/index')?>">
							检索评论
						</a>
					</li>					
				</ul>
			</li> -->
			


			<li <?php if( 'Goods'=='System'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-exclamation-sign icon-white"></i></div>
					系统管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='System'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='System' && 'cateImgList'=='linkList' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/linkList')?>">
							友情链接
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'cateImgList'=='UserFace' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/UserFace')?>">
							修改头像
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'cateImgList'=='editUser' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/editUser')?>">
							修改密码
						</a>
					</li>
					<li <?php if( 'Goods'=='System' && 'cateImgList'=='systemSet' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('System/systemSet')?>">
							网站设置
						</a>
					</li>
				</ul>
			</li>
		
			

			 <li <?php if( 'Goods'=='Rbac'){?>
                class='active open'
               <?php }?>>
				<a href="#" class='light toggle-collapsed'>
					<div class="ico"><i class="icon-tasks icon-white"></i></div>
					RBAC权限管理
					<img src="<?php echo __PUBLIC__?>/Admin/img/toggle-subnav-down.png" alt="">
				</a>
				<ul <?php if( 'Goods'=='Rbac'){?>
                class='collapsed-nav'<?php }else{?>class='collapsed-nav closed'
               <?php }?>>
					<li <?php if( 'Goods'=='Rbac' && 'cateImgList'=='index' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/index')?>">
							用户列表
						</a>
					</li>
					
					<li <?php if( 'Goods'=='Rbac' && 'cateImgList'=='Rolelist' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/Rolelist')?>">
							角色列表
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'cateImgList'=='addRole' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/addRole')?>">
							添加角色
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'cateImgList'=='nodelist' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/nodelist')?>">
							节点列表
						</a>
					</li>
					<li <?php if( 'Goods'=='Rbac' && 'cateImgList'=='addNode' ){?>
                class='active'
               <?php }?>>
						<a href="<?php echo U('Rbac/addNode')?>">
							添加节点
						</a>
					</li>
				</ul>
			</li>
	

			
		</ul>
	</div>
	<!--左边的导航 结束-->

	<div class="content">
			<div class="row-fluid">
				<div class="span12">
					<div class="box">
						<div class="box-head">
							<h3><button class="btn btn-blue7">栏目图片列表</button></h3>
						</div>
						<div>
						<button class="btn btn-success" style="float:left;margin:5px 14px;" onclick='location.href="<?php echo U('cateImgedit')?>"'>+添加图片</button>
						</div>
						<div class="box-content box-nomargin">
										<table class='table table-striped dataTable table-bordered dataTable-tools'>
											<thead>
												<tr>
													<th>ID</th>
													<th>首页栏目图片</th>
													<th>列表栏目图片</th>
													<th>栏目级别</th>
													<th>分类名称</th>
													<th>操作</th>
												</tr>
											</thead>
											<tbody>
											<?php foreach ($listData as $v){?>
												<tr class="delTr">
													<td  style="text-align:center;"><?php echo $v['cid']?></td>
													<td class="table-image"  style="text-align:center;">
														<a href="<?php echo __ROOT__?>/<?php echo $v['max_imgs_250x130']?>" class="preview fancy"><img src="<?php echo __ROOT__?>/<?php echo $v['max_imgs_250x130']?>" alt="" width="80" height="80"></a>
													</td>
													<td class="table-image"  style="text-align:center;">
														<a href="<?php echo __ROOT__?>/<?php echo $v['max_imgs_200x240']?>" class="preview fancy"><img src="<?php echo __ROOT__?>/<?php echo $v['max_imgs_200x240']?>" alt="" width="80" height="80"></a>
													</td>
													<td  style="text-align:center;">一级栏目</td>
													<td  style="text-align:center;"><?php echo $v['cname']?></td>
													<td style="text-align:center;">
														<div class="btn-group">
															<a href="<?php echo U('twoCateList',array('cid'=>$v['cid']))?>" class="btn btn-mini tip " title="查看子栏目图片">
																<img src="<?php echo __PUBLIC__?>/Admin/img/icons/essen/16/document-library.png" alt="查看子栏目图片">
															</a>													
															<a href="<?php echo U('cateImgShow',array('cid'=>$v['cid'],'lev'=>$v['_level']))?>" class="btn btn-mini tip " title="修改">
																<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/document-task.png" alt="修改">
															</a>
															<a href="javascript:;" class="btn btn-mini tip cate_delete"   title="删除" cid="<?php echo $v['cid']?>" lev="1">
																<img src="<?php echo __PUBLIC__?>/Admin/img/icons/fugue/cross.png" alt="删除">
															</a>
														</div>			
													</td>
												</tr>
											<?php }?>
											
											<tr>
											<td colspan="6">
												<div class="dataTables_paginate paging_bootstrap pagination" id="pages">
													<?php echo $page?>
												</div>
												</td>
											</tr>
											</tbody>
											
										</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>	

<!--载入尾部-->
<script src="<?php echo __PUBLIC__?>/Admin/js/jquery.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/common.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/goods.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/mgjs/formValidate.js"></script>
<script src="<?php echo __PUBLIC__?>/hdjs/hdjs.min.js"></script>

<script src="<?php echo __PUBLIC__?>/Admin/js/bootstrap.min.js"></script>


<script src="<?php echo __PUBLIC__?>/Admin/js/admin.js"></script>
<!-- <script src="<?php echo __PUBLIC__?>/Admin/My97DatePicker/WdatePicker.js"></script> -->
<script src="<?php echo __PUBLIC__?>/Admin/js/jquery.fancybox.js"></script>

<script type="text/javascript" src="<?php echo __PUBLIC__?>/Uploadify/jquery.uploadify.min.js"></script>
<script src="<?php echo __PUBLIC__?>/Admin/js/custom.js"></script>


</body>
</html>
